from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'transaksi_pembelian'

urlpatterns = [
    path('', views.transaksi_pembelian, name="list"),
    path('form', views.form_transaksi_pembelian, name="form"),
    path('register', views.register_transaksi_pembelian, name="register"),
    path('delete', views.delete_transaksi_pembelian, name="delete"),
    path('tambah', views.tambah_transaksi_pembelian, name="tambah")]