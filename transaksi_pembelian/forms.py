from django import forms
from django.db import connection

konsumen_choices = []
with connection.cursor() as cursor:
    cursor.execute('SELECT id_konsumen FROM transaksi_pembelian')
    row = cursor.fetchall()
for id_produk in row:
    konsumen_choices.append((id_produk[0], id_produk[0]))

class TransaksiPembelianForm(forms.Form):
 
    id_konsumen = forms.ChoiceField(label="ID Konsumen", widget=forms.Select(), 
        choices=konsumen_choices, initial=konsumen_choices[0][0])
