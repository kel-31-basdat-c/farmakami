from django.shortcuts import render, redirect
from datetime import datetime
from django.db import connection
from .forms import TransaksiPembelianForm
import random

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def transaksi_pembelian(request):
    try :
            
        if 'auth' in request.session:
            if request.session['role'] == 'konsumen':
                email = request.session['auth']
                with connection.cursor() as cursor:
                    command = "SELECT TP.id_transaksi_pembelian, TP.waktu_pembelian, TP.id_konsumen, TP.total_pembayaran  FROM transaksi_pembelian TP, konsumen KON WHERE KON.email = '" + email+ "' AND KON.id_konsumen = TP.id_konsumen"
                    cursor.execute(command)
                    transaksi_pembelian = dictfetchall(cursor)
                    kosong = False
                    if len(transaksi_pembelian) == 0:
                        kosong = True
                context = {
                    'transaksi_pembelian':transaksi_pembelian, 
                    'is_admin': request.session['role'] == 'admin',
                    'kosong' : kosong
                }
                return render(request, 'transaksipembelian.html', context)
            else:
                with connection.cursor() as cursor:
                    cursor.execute('SELECT * FROM transaksi_pembelian')
                    transaksi_pembelian = dictfetchall(cursor)
                    kosong = False
                    if len(transaksi_pembelian) == 0:
                        kosong = True
                context = {
                    'transaksi_pembelian':transaksi_pembelian, 
                    'is_admin': request.session['role'] == 'admin',
                    'kosong' : kosong
                }
                return render(request, 'transaksipembelian.html', context)
        else:
            return redirect('authentication:login')
    
    
    except :
        return render(request, 'notfound.html')
    
def tambah_transaksi_pembelian(request):
    return redirect('transaksi_pembelian:list')

def form_transaksi_pembelian(request):
    try :

        if 'auth' in request.session:
            context = {'is_admin': request.session['role'] == 'admin'}
            if request.method == 'POST':
                context['form'] = TransaksiPembelianForm(request.POST)
                context['id_transaksi_pembelian'] = request.POST['id_transaksi_pembelian']
                context['id_konsumen'] = request.POST['id_konsumen']
                context['waktu_pembelian'] = request.POST['waktu_pembelian']
                context['total_pembayaran'] = request.POST['total_pembayaran']
                context['action'] = 'Update'
            else:
                context['form'] =TransaksiPembelianForm()
                context['action'] = 'Create'
            return render(request, 'form_transaksi_pembelian.html', context)
        else:
            return redirect('authentication:login')

    except :
        return render(request, 'notfound.html')

def register_transaksi_pembelian(request):
    try : 
        if request.method == 'POST':
            context = { 
                'form': TransaksiPembelianForm(request.POST),
                'is_admin': request.session['role'] == 'admin'
            }
            if request.POST['action'] == 'Update':
                id_transaksi_pembelian = request.POST['old_id_transaksi_pembelian']
                waktu_pembelian = request.POST['old_waktu_pembelian']
                total_pembayaran = request.POST['old_total_pembayaran']
                id_konsumen = request.POST['id_konsumen']
                old_id_konsumen = request.POST['old_id_konsumen']
                print("1. " + id_konsumen + " 2. " + old_id_konsumen + " 3. " + request.POST['action'])
                new_waktu_pembelian = datetime.now()
                with connection.cursor() as cursor:
                    cursor.execute('UPDATE transaksi_pembelian SET ' \
                        + 'id_transaksi_pembelian = %s, waktu_pembelian = %s, ' \
                        + 'total_pembayaran = %s, id_konsumen = %s' \
                        + 'WHERE id_konsumen = %s', \
                        [id_transaksi_pembelian, new_waktu_pembelian, total_pembayaran, id_konsumen, old_id_konsumen])



            elif request.POST['action'] == 'Create':
                id_transaksi_pembelian = "TRP-" + str(int(random.randint(10000,99999)))
                id_konsumen = request.POST['id_konsumen']
                new_waktu_pembelian = datetime.now()
                total_pembayaran = 0

                with connection.cursor() as cursor:
                    cursor.execute('INSERT INTO transaksi_pembelian VALUES (%s, %s, %s, %s)', \
                    [id_transaksi_pembelian, new_waktu_pembelian, id_konsumen, total_pembayaran])
            return redirect('transaksi_pembelian:list')
        else:
            return redirect('produk_apotek:form')
    
    except :
        return render(request, 'notfound.html')

def delete_transaksi_pembelian(request):
    try :
        id_transaksi_pembelian = request.POST['id_transaksi_pembelian']
        with connection.cursor() as cursor:
            cursor.execute('DELETE FROM transaksi_pembelian WHERE id_transaksi_pembelian = %s ', \
                [id_transaksi_pembelian])
        return redirect('transaksi_pembelian:list')

    except :
        return render(request, 'notfound.html')