from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'list_produk_dibeli'

urlpatterns = [
    path('', views.list_produk_dibeli, name="list"),
    path('form', views.form_list_produk_dibeli, name="form"),
    path('register', views.register_list_produk_dibeli, name="register"),
    path('delete', views.delete_list_produk_dibeli, name="delete"),
]