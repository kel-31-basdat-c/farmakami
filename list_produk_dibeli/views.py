from django.shortcuts import render, redirect
from django.db import connection
from .forms import *

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def list_produk_dibeli(request):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM list_produk_dibeli')
        list_produk_dibeli = dictfetchall(cursor)
    return render(request, 'list-produk-dibeli.html', {'list_produk_dibeli':list_produk_dibeli})

def form_list_produk_dibeli(request):
    context = {}
    
    if request.method == 'POST':
        form = ListProdukDibeliForm(request.POST)
        form.fields['id_apotek'].widget = forms.HiddenInput()
        form.fields['id_produk'].widget = forms.HiddenInput()
        context['form'] = form
        context['id_produk'] = request.POST['id_produk']
        context['id_apotek'] = request.POST['id_apotek']
        context['id_transaksi_pembelian'] = request.POST['id_transaksi_pembelian']
        context['action'] = 'Update'

    else:
        context['form'] = ListProdukDibeliForm()
        context['action'] = 'Create'
    return render(request, 'form-list-produk-dibeli.html', context)

def register_list_produk_dibeli(request):
    if request.method == 'POST':
        context = { 'form': ListProdukDibeliForm(request.POST) }
        id_produk = request.POST['id_produk']
        id_apotek = request.POST['id_apotek']
        id_transaksi_pembelian = request.POST['id_transaksi_pembelian']
        jumlah = request.POST['jumlah']

        # check if primary key exists => error
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM list_produk_dibeli WHERE id_apotek = %s AND id_produk = %s AND id_transaksi_pembelian = %s',
            [id_apotek, id_produk, id_transaksi_pembelian])
            row = cursor.fetchone()
        if row:
            if request.POST['action'] == 'Create':
                context['error'] = "Antara ID produk dan ID apotek atau ID Transaksi sudah terambil."
                context['action'] = request.POST['action']
                return render(request, 'form-list-produk-dibeli.html', context)

        if request.POST['action'] == 'Update':
            old_id_transaksi_pembelian = request.POST['old_id_transaksi_pembelian']
            with connection.cursor() as cursor:
                cursor.execute('UPDATE list_produk_dibeli SET ' \
                    + 'id_apotek = %s, id_produk = %s, ' \
                    + 'id_transaksi_pembelian = %s, ' \
                    + 'jumlah = %s ' \
                    + 'WHERE id_apotek = %s AND id_produk = %s AND id_transaksi_pembelian = %s', \
                    [id_apotek, id_produk, id_transaksi_pembelian, jumlah, 
                    id_apotek, id_produk, old_id_transaksi_pembelian])
        elif request.POST['action'] == 'Create':
            with connection.cursor() as cursor:
                cursor.execute('INSERT INTO list_produk_dibeli(id_apotek, id_produk, id_transaksi_pembelian, jumlah) VALUES (%s, %s, %s, %s)', \
                    [id_apotek, id_produk, id_transaksi_pembelian, jumlah])
        return redirect('list_produk_dibeli:list')
    else:
        return redirect('list_produk_dibeli:form')

def delete_list_produk_dibeli(request):
    id_produk = request.POST['id_produk']
    id_apotek = request.POST['id_apotek']
    id_transaksi_pembelian = request.POST['id_transaksi_pembelian']
    with connection.cursor() as cursor:
        cursor.execute('DELETE FROM list_produk_dibeli WHERE id_apotek = %s AND id_produk = %s AND id_transaksi_pembelian = %s', \
            [id_apotek, id_produk, id_transaksi_pembelian])
    return redirect('list_produk_dibeli:list')