from django import forms
from django.db import connection

produk_choices = []
with connection.cursor() as cursor:
    cursor.execute('SELECT id_produk FROM produk')
    row = cursor.fetchall()
for id_produk in row:
    produk_choices.append((id_produk[0], id_produk[0]))

apotek_choices = []
with connection.cursor() as cursor:
    cursor.execute('SELECT id_apotek FROM apotek')
    row = cursor.fetchall()
for id_apotek in row:
    apotek_choices.append((id_apotek[0], id_apotek[0]))

transaksi_choices = []
with connection.cursor() as cursor:
    cursor.execute('SELECT id_transaksi_pembelian FROM transaksi_pembelian')
    row = cursor.fetchall()
for id_transaksi_pembelian in row:
    transaksi_choices.append((id_transaksi_pembelian[0], id_transaksi_pembelian[0]))

class ListProdukDibeliForm(forms.Form):
 
    id_produk = forms.ChoiceField(label="ID Produk", widget=forms.Select(), 
        choices=produk_choices, initial=produk_choices[0][0])
    id_apotek = forms.ChoiceField(label="ID Apotek", widget=forms.Select(), 
        choices=apotek_choices, initial=apotek_choices[0][0])
    id_transaksi_pembelian = forms.ChoiceField(label="ID Transaksi Pembelian", widget=forms.Select(), 
        choices=transaksi_choices, initial=transaksi_choices[0][0])
    jumlah = forms.CharField(label="Jumlah", required=True, widget=forms.NumberInput(), initial=0)