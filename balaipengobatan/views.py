from django.shortcuts import render, redirect
from django.db import connection
from django.utils.datastructures import MultiValueDictKeyError

# Create your views here.

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def daftar_balai(request):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM balai_pengobatan ORDER BY nama_balai ASC')
        balai_pengobatan = dictfetchall(cursor)
        
        cursor.execute('SELECT * FROM balai_apotek b, apotek a WHERE b.id_apotek = a.id_apotek')
        balai_apotek = dictfetchall(cursor)

    result = {
        'balai_pengobatan': balai_pengobatan,
        'balai_apotek' : balai_apotek
    }
    return render(request, 'balaipengobatan.html', result)

def tambah_balai(request):
    with connection.cursor() as cursor:
        cursor.execute('SELECT id_apotek, nama_apotek FROM apotek ORDER BY nama_apotek ASC')
        list_apotek_asosiasi = dictfetchall(cursor)
        banyak_apotek = len(list_apotek_asosiasi)

        cursor.execute('SELECT id_balai FROM balai_pengobatan ORDER BY id_balai DESC LIMIT 1')
        last_id = cursor.fetchone()[0]

        cursor.execute('SELECT alamat_balai FROM balai_pengobatan')
        list_alamat = dictfetchall(cursor)

        # min ascii A = 65, max ascii Z = 90
        counter = int(last_id[6:]) + 1
        idbalai = ""
        if counter == 1000:
            counter -= 1000
            new = chr(ord(last_id[4]) + 1)
            idbalai = last_id[:4] + str(new) + ('%0.3d' % counter)
            if ord(new) == 91 :
                new = 'A'
                new2 = chr(ord(last_id[3]) + 1)
                idbalai = last_id[:3] + str(new2) + str(new) + ('%0.3d' % counter)
        else:
            idbalai = last_id[:6] + str(counter)
    baru = []
    for i in list_alamat:
        baru.append(i.get("alamat_balai"))

    print(baru)
    temp = idbalai

    if request.method == 'POST':
        id_balai = temp
        alamat = request.POST['alamat']
        nama = request.POST['nama']
        jenis = request.POST['jenis']
        telepon = request.POST['telepon']
        asosiasi_apotek = []
        for i in range(banyak_apotek):
            cb = 'cb' + str(i)
            id_ap = 'id' + str(i)
            temp_cb = request.POST[cb]
            temp_id = request.POST[id_ap]
            if temp_cb == 'on':
                asosiasi_apotek.append(temp_id)

        if alamat not in baru:
            with connection.cursor() as cursor:
                cursor.execute('INSERT INTO balai_pengobatan VALUES (%s, %s, %s, %s, %s)', [id_balai, alamat, nama, jenis, telepon])
                for item in asosiasi_apotek:
                    cursor.execute('INSERT INTO balai_apotek VALUES (%s, %s)', [id_balai, item[:9]])
            return redirect('balaipengobatan:daftar_balai')
        
        is_error = True
        pesan = {
            'list_apotek_asosiasi' : list_apotek_asosiasi,
            'is_error' : is_error
        }
        return render(request, 'tambahbalai.html', pesan)
    else:
        daftar = {
            'list_apotek_asosiasi' : list_apotek_asosiasi,
        }
    return render(request, 'tambahbalai.html', daftar)

def update_balai(request):
    try:
        if request.method == 'GET':
            id_balai = request.GET['id_balai']
        else:
            id_balai = request.POST['id_balai']
    except:
        return render(request, 'updatebalai.html', {'is_failed' : True})

    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM apotek ORDER BY nama_apotek ASC')
        list_update_apotek = dictfetchall(cursor)
        banyak_apotek = len(list_update_apotek)

        cursor.execute('SELECT alamat_balai FROM balai_pengobatan')
        list_alamat = dictfetchall(cursor)

        cursor.execute('SELECT * FROM balai_pengobatan  WHERE id_balai = %s', \
            [id_balai])
        list_update = dictfetchall(cursor)

        cursor.execute('SELECT id_apotek FROM balai_apotek WHERE id_balai = %s', [id_balai])
        temp = dictfetchall(cursor)

        cursor.execute('SELECT id_balai FROM balai_pengobatan')
        temp2 = dictfetchall(cursor)

        list_asosiasi = []
        for i in temp:
            list_asosiasi.append(i.get('id_apotek'))

        list_balai = []
        for j in temp2:
            list_balai.append(j.get('id_balai'))

    baru = []
    for i in list_alamat:
        baru.append(i.get("alamat_balai"))

    if id_balai not in list_balai:
        return render(request, 'updatebalai.html', {'is_failed' : True})

    if request.method == 'POST':
        old_alamat = request.POST['old_alamat']
        alamat = request.POST['alamat']
        nama = request.POST['nama']
        jenis = request.POST['jenis']
        telepon = request.POST['telepon']
        asosiasi_apotek = []
        for i in range(banyak_apotek):
            cb = 'cb' + str(i)
            id_ap = 'id' + str(i)
            temp_cb = request.POST[cb]
            temp_id = request.POST[id_ap]
            if temp_cb == 'on':
                asosiasi_apotek.append(temp_id)
        baru.remove(old_alamat)
        
        if alamat not in baru:
            with connection.cursor() as cursor:
                cursor.execute('UPDATE balai_pengobatan SET ' \
                    + 'alamat_balai = %s, nama_balai = %s, ' \
                    + 'jenis_balai = %s, telepon_balai = %s '\
                    + 'WHERE id_balai = %s', [alamat, nama, jenis, telepon, id_balai])
                cursor.execute('DELETE FROM balai_apotek WHERE id_balai = %s', [id_balai])
                for item in asosiasi_apotek:
                    cursor.execute('INSERT INTO balai_apotek VALUES (%s, %s)', [id_balai, item[:9]])
            return redirect('balaipengobatan:daftar_balai')

        is_error = True
        pesan = {
            'list_update' : list_update,
            'list_update_apotek' : list_update_apotek,
            'list_apotek_asosiasi' : list_asosiasi,
            'is_error' : is_error,
            'idbalai' : id_balai
        }
        return render(request, 'updatebalai.html', pesan)

    daftar_update = {
        'list_update' : list_update,
        'list_update_apotek' : list_update_apotek,
        'list_apotek_asosiasi' : list_asosiasi,
        'idbalai' : id_balai
    }
    return render(request, 'updatebalai.html', daftar_update)

def delete_balai(request):
    try:
        id_balai = request.POST['id_balai']
        with connection.cursor() as cursor:
            cursor.execute('DELETE FROM balai_pengobatan WHERE id_balai = %s', [id_balai])
        return redirect('balaipengobatan:daftar_balai')
    except:
        return render(request, 'notfound.html')