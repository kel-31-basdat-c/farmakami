from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'balaipengobatan'

urlpatterns = [
    path('', views.daftar_balai, name='daftar_balai'),
    path('create', views.tambah_balai, name='tambah_balai'),
    path('update', views.update_balai, name='update_balai'),
    path('delete', views.delete_balai, name="delete_balai")
    # dilanjutkan ...
]