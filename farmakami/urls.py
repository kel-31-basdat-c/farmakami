from django.urls import path, include

urlpatterns = [
    path('apotek/', include('apotek.urls')),
    path('produk-apotek/', include('produk_apotek.urls')),
    path('balai-pengobatan/', include('balaipengobatan.urls')),
    path('obat/', include('obat.urls')),
    path('transaksipembelian/', include('transaksi_pembelian.urls')),
    path('list-produk-dibeli/', include('list_produk_dibeli.urls')),
    path('pengantaran-farmasi/', include('pengantaran_farmasi.urls')),
    path('', include('profil.urls')),
    path('signup/', include('signup.urls')),
    path('', include('authentication.urls')),
]

handler404 = 'authentication.views.handler404'
handler500 = 'authentication.views.handler500'