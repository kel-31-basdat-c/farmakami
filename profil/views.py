from django.shortcuts import render, redirect
from django.db import connection

def profil_anda(request) :
    if 'auth' not in request.session :
        return redirect('authentication:login')


    email = request.session['auth']
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM pengguna WHERE email = %s', [email])
        row = cursor.fetchone()

    telepon = row[1]
    if telepon == None :
        telepon = '-'

    data_pengguna = {'nama_lengkap' : row[3], 'tipe' : row[4], 
                   'telepon' : telepon, 'email' : row[0]}

    if 'konsumen' in row[4]:
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM konsumen WHERE email = %s', [email])
            konsumen = cursor.fetchone()
            try :
                data_pengguna = {'nama_lengkap' : row[3], 'tipe' : row[4], 
                        'telepon' : telepon, 'email' : row[0], 
                        'id_konsumen' : konsumen[0], 'jenis_kelamin' : konsumen[2], 
                        'tanggal_lahir' : konsumen[3]}
            except:
                pass
    
    elif row[4] == 'kurir':
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM kurir WHERE email = %s', [email])
            kurir = cursor.fetchone()
            try :
                data_pengguna = {'nama_lengkap' : row[3], 'tipe' : row[4], 
                        'telepon' : telepon, 'email' : row[0], 
                        'id_kurir' : kurir[0],
                        'nama_perusahaan' : kurir[2]}
            except :
                pass


    elif row[4] == 'admin':
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM admin_apotek WHERE email = %s', [email])
            admin_apotek = cursor.fetchone()
            try :
                data_pengguna = {'nama_lengkap' : row[3], 'tipe' : row[4], 
                        'telepon' : telepon, 'email' : row[0], 
                        'no_apotek' : admin_apotek[1]}
            except :
                pass
    elif row[4] == 'cs':
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM cs WHERE email = %s', [email])
            cs = cursor.fetchone()
            try :
                    data_pengguna = {'nama_lengkap' : row[3], 'tipe' : row[4], 
                            'telepon' : telepon, 'email' : row[0], 
                            'no_ktp' : cs[0], 'no_sia' : cs[2]}
            except :
                pass

    """
    data_pengguna = {'nama_lengkap' : row[3], 'tipe' : row[4], 
                    'telepon' : telepon, 'email' : row[0], 
                    'id_konsumen' : konsumen[0], 'jenis_kelamin' : konsumen[2], 
                    'tanggal_lahir' : konsumen[3], 'id_kurir' : kurir[0],
                    'nama_perusahaan' : kurir[2], 'no_apotek' : admin_apotek[1],
                    'no_ktp' : cs[0], 'no_sia' : cs[2]}
    """
    return render(request, 'profil.html', {'data_pengguna' : data_pengguna})
