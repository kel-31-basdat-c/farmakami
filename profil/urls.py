from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'profil'

urlpatterns = [
    path('', views.profil_anda, name="profil"),
]