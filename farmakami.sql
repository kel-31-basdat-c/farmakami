
---
--- Farmakami DB
--- 

DROP SCHEMA IF EXISTS FARMAKAMI CASCADE;

CREATE SCHEMA FARMAKAMI;

SET search_path to FARMAKAMI;

CREATE TABLE PENGGUNA(
    email VARCHAR(50),
    telepon VARCHAR(20),
    password VARCHAR(128) NOT NULL,
    nama_lengkap VARCHAR(50) NOT NULL,
    tipe VARCHAR(8),
    PRIMARY KEY (email)
);

INSERT INTO PENGGUNA VALUES
    ('aculcheth0@mlb.com',1602812881.0,'aJFL8gTUiE','Aldis Culcheth','konsumen'),
    ('cgaynor1@live.com',3538619571.0,'TZq9vgI1G','Chaddie Gaynor','admin'),
    ('lbroader2@google.pl',8994355514.0,'SGaOkY5gQrv','Livia Broader','konsumen'),
    ('hhulance3@miibeian.gov.cn',8391597948.0,'F3l0dOyMUgXt','Haroun Hulance','konsumen'),
    ('gesley4@desdev.cn',4717132178.0,'RzLEkesFbUR','Gene Esley','admin'),
    ('urigge5@slate.com',6668641999.0,'lS6lKyEL','Udell Rigge','cs'),
    ('cnorquoy6@marriott.com',6296049213.0,'fFbkxkOU','Clair Norquoy','admin'),
    ('lzanni7@exblog.jp',3723244990.0,'rHCYlvcNU','Lorrayne Zanni','konsumen'),
    ('dmaylin8@so-net.ne.jp',NULL,'OTsMqb5','Deeanne Maylin','konsumen'),
    ('rminney9@gmpg.org',3949033483.0,'GQWgI25','Ranique Minney','konsumen'),
    ('schedzoya@livejournal.com',1695807517.0,'sNqrjl','Sansone Chedzoy','konsumen'),
    ('rkarmelb@latimes.com',4687184513.0,'MNCHcLxMbYal','Rodina Karmel','konsumen'),
    ('ccarrellc@netscape.com',1094197817.0,'nQhZBtt','Concettina Carrell','konsumen'),
    ('tharcased@bigcartel.com',NULL,'dTZKoJJv','Theresina Harcase','admin'),
    ('pyeende@flickr.com',3364359852.0,'ctZRpFfIcqD','Pembroke Yeend','cs'),
    ('chazlef@creativecommons.org',7649143202.0,'tCdR2I772','Carolan Hazle','kurir'),
    ('sbarberag@vk.com',2674558510.0,'e8SahJHX6c','Sisely Barbera','cs'),
    ('apuddinh@parallels.com',4108471104.0,'w6CVhuQmyA','Albert Puddin','kurir'),
    ('ctouni@reference.com',4367361351.0,'cNrGyqGCHg','Claudian Toun','kurir'),
    ('ehollyerj@pinterest.com',7673701708.0,'RzXOG1Q4Y','Elijah Hollyer','kurir'),
    ('hemilienk@walmart.com',4755116004.0,'0QhZk9qbXI','Herminia Emilien','cs'),
    ('hpeascodl@wufoo.com',6106736962.0,'vQcZze','Hurlee Peascod','kurir'),
    ('jsizlandm@google.com.br',6877893142.0,'RjHnJcBGby','Judi Sizland','kurir'),
    ('gsparrown@ycombinator.com',NULL,'4IlgXE5UvN','Gustav Sparrow','kurir'),
    ('jdellentyo@ft.com',8923594171.0,'BjXDTZ','Janessa Dellenty','kurir'),
    ('ntattoop@reddit.com',3155170783.0,'XBt7a4YG0z','Nalani Tattoo','kurir'),
    ('dolifardq@myspace.com',6081028758.0,'9uhO225Ddu','Dido Olifard','admin'),
    ('lbendar@redcross.org',7288633015.0,'M155lt','Livvyy Benda','admin'),
    ('jmillions@elegantthemes.com',7405038013.0,'xKjkVp6X48','Julius Million','cs'),
    ('ldaoustt@angelfire.com',9999817559.0,'nyTJCaayQnMU','Latrina Daoust','cs'),
    ('asrieldreemur@undertale.com',82293631145,'whatever','Asriel Dreemur','admin');


CREATE TABLE KONSUMEN(
    id_konsumen VARCHAR(10),
    email VARCHAR(50) NOT NULL,
    jenis_kelamin VARCHAR(1) NOT NULL,
    tanggal_lahir DATE NOT NULL,
    PRIMARY KEY (id_konsumen),
    FOREIGN KEY (email) REFERENCES
    PENGGUNA(email) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO KONSUMEN VALUES
    ('K-byz5n4','aculcheth0@mlb.com','M','1996-07-16 00:00:00'),
    ('K-j3xnl2','lbroader2@google.pl','F','2002-10-10 00:00:00'),
    ('K-f1efdj','hhulance3@miibeian.gov.cn','F','1984-04-12 00:00:00'),
    ('K-qtnljr','gesley4@desdev.cn','M','1993-01-26 00:00:00'),
    ('K-vfvc8s','lzanni7@exblog.jp','F','1980-08-22 00:00:00'),
    ('K-j0ztog','dmaylin8@so-net.ne.jp','M','2008-11-08 00:00:00'),
    ('K-rneg2u','rminney9@gmpg.org','M','1996-06-05 00:00:00'),
    ('K-3j17ku','schedzoya@livejournal.com','F','1994-10-13 00:00:00'),
    ('K-hk58bb','rkarmelb@latimes.com','F','2009-10-19 00:00:00'),
    ('K-x08117','ccarrellc@netscape.com','F','1992-08-21 00:00:00');


CREATE TABLE ALAMAT_KONSUMEN(
    id_konsumen VARCHAR(10),
    alamat TEXT,
    status VARCHAR(20),
    PRIMARY KEY (id_konsumen, alamat, status),
    FOREIGN KEY (id_konsumen) REFERENCES
    KONSUMEN(id_konsumen) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO ALAMAT_KONSUMEN VALUES
    ('K-3j17ku','9348 Starling Street','aktif'),
    ('K-hk58bb','44788 5th Avenue','aktif'),
    ('K-x08117','47 Anzinger Alley','aktif'),
    ('K-byz5n4','49 Buena Vista Park','nonaktif'),
    ('K-j3xnl2','6 Chinook Street','nonaktif'),
    ('K-f1efdj','9 Clove Court','nonaktif'),
    ('K-rneg2u','57048 North Pass','nonaktif'),
    ('K-hk58bb','736 Service Plaza','nonaktif'),
    ('K-x08117','2 Clemons Lane','nonaktif'),
    ('K-qtnljr','3804 Bluestem Plaza','aktif'),
    ('K-vfvc8s','91 Maple Wood Circle','aktif'),
    ('K-byz5n4','93 Center Lane','nonaktif'),
    ('K-j3xnl2','90935 4th Hill','nonaktif'),
    ('K-j0ztog','3 Mandrake Plaza','aktif'),
    ('K-rneg2u','7 Toban Center','aktif'),
    ('K-j0ztog','198 Carioca Avenue','nonaktif'),
    ('K-f1efdj','370 Esch Road','nonaktif'),
    ('K-byz5n4','39373 Banding Avenue','aktif'),
    ('K-j3xnl2','9 Vera Parkway','aktif'),
    ('K-f1efdj','1618 Waubesa Drive','aktif');


CREATE TABLE APOTEKER(
    email VARCHAR(50),
    PRIMARY KEY (email),
    FOREIGN KEY (email) REFERENCES
    PENGGUNA (email)
);

INSERT INTO APOTEKER VALUES
    ('dolifardq@myspace.com'),
    ('lbendar@redcross.org'),
    ('jmillions@elegantthemes.com'),
    ('ldaoustt@angelfire.com'),
    ('tharcased@bigcartel.com'),
    ('pyeende@flickr.com'),
    ('sbarberag@vk.com'),
    ('hemilienk@walmart.com'),
    ('urigge5@slate.com'),
    ('cnorquoy6@marriott.com'),
    ('cgaynor1@live.com'),
    ('asrieldreemur@undertale.com');


CREATE TABLE CS(
    no_ktp VARCHAR(20),
    email VARCHAR(50) NOT NULL,
    no_sia VARCHAR(20) NOT NULL UNIQUE,
    PRIMARY KEY (no_ktp),
    FOREIGN KEY (email) REFERENCES
    APOTEKER(email)
);

INSERT INTO CS VALUES
    (8.36032630121e+19,'jmillions@elegantthemes.com',560773393961),
    (6.13481872159e+19,'ldaoustt@angelfire.com',242554472161),
    (9.61180211631e+19,'pyeende@flickr.com',526339767458),
    (1.64124958267e+19,'sbarberag@vk.com',411782335646),
    (2.53272166317e+19,'hemilienk@walmart.com',310986210547),
    (4.79938610699e+19,'urigge5@slate.com',558218539345);


CREATE TABLE APOTEK(
    id_apotek VARCHAR(10),
    email VARCHAR(50) NOT NULL UNIQUE,
    no_sia VARCHAR(20) NOT NULL UNIQUE,
    nama_penyelenggara VARCHAR(50) NOT NULL,
    nama_apotek VARCHAR(50) NOT NULL,
    alamat_apotek TEXT NOT NULL UNIQUE,
    telepon_apotek VARCHAR(20),
    PRIMARY KEY (id_apotek)
);

INSERT INTO APOTEK VALUES
    ('APTK-0001','lfeely2@newsvine.com',728057919056,'Leticia Feely','Apotek Kunang','2247 International Street',9592330185),
    ('APTK-0002','jcarpe3@g.co',706029120448,'Jacinta Carpe','Mitra Keluarga','94 Nobel Circle',3672539441),
    ('APTK-0003','lmartelet4@umich.edu',283848307714,'Lionello Martelet','Big Pharma','6802 Hoffman Crossing',4334255953),
    ('APTK-0004','lbront5@ed.gov',211537629874,'Ludwig Bront','Anti Disease','9590 Trailsway Hill',8831682012),
    ('APTK-0005','rmoulin6@1688.com',878359545208,'Raleigh Moulin','Harapan Keluarga','2 Schurz Avenue',5353200324),
    ('APTK-0006','asikorsky7@engadget.com',562300119798,'Abbe Sikorsky','Siloam','95520 Northwestern Lane',7218578678);


CREATE TABLE ADMIN_APOTEK(
    email VARCHAR(50),
    id_apotek VARCHAR(10),
    PRIMARY KEY (email),
    FOREIGN KEY (email) REFERENCES
    APOTEKER(email),
    FOREIGN KEY (id_apotek) REFERENCES
    APOTEK (id_apotek)
);

INSERT INTO ADMIN_APOTEK VALUES
    ('dolifardq@myspace.com','APTK-0001'),
    ('lbendar@redcross.org','APTK-0002'),
    ('tharcased@bigcartel.com','APTK-0003'),
    ('cnorquoy6@marriott.com','APTK-0004'),
    ('asrieldreemur@undertale.com','APTK-0005'),
    ('cgaynor1@live.com','APTK-0006');


CREATE TABLE KURIR(
    id_kurir VARCHAR(10),
    email VARCHAR(50) NOT NULL,
    nama_perusahaan VARCHAR(50) NOT NULL,
    PRIMARY KEY (id_kurir),
    FOREIGN KEY (email) REFERENCES
    PENGGUNA (email)
);

INSERT INTO KURIR VALUES
    ('KR-LX330','chazlef@creativecommons.org','Heathcote Group'),
    ('KR-MM270','apuddinh@parallels.com','Heaney-Shanahan'),
    ('KR-RO953','ctouni@reference.com','Rutherford Group'),
    ('KR-HC124','ehollyerj@pinterest.com','Rogahn-Schulist'),
    ('KR-IR572','hpeascodl@wufoo.com','Gibson-Sauer'),
    ('KR-OO526','jsizlandm@google.com.br','Deckow Inc'),
    ('KR-WT987','gsparrown@ycombinator.com','Kilback, Watsica and Schmidt'),
    ('KR-SM993','jdellentyo@ft.com','Bayer, Breitenberg and Johns'),
    ('KR-LU226','ntattoop@reddit.com','Jacobson, Morar and Berge');


CREATE TABLE TRANSAKSI_PEMBELIAN(
    id_transaksi_pembelian VARCHAR(10),
    waktu_pembelian TIMESTAMP NOT NULL,
    id_konsumen VARCHAR(10) NOT NULL,
    total_pembayaran INTEGER NOT NULL,
    PRIMARY KEY (id_transaksi_pembelian),
    FOREIGN KEY (id_konsumen) REFERENCES
    KONSUMEN (id_konsumen)
);

INSERT INTO TRANSAKSI_PEMBELIAN VALUES
    ('TRP-60036','2018-01-10 06:07:18','K-hk58bb',14000),
    ('TRP-09359','2019-01-29 11:55:47','K-qtnljr',120000),
    ('TRP-12043','2020-03-15 05:06:15','K-f1efdj',40000),
    ('TRP-62799','2017-04-13 07:01:35','K-byz5n4',21000),
    ('TRP-26224','2017-07-04 13:52:28','K-j3xnl2',45000),
    ('TRP-69051','2015-07-08 13:41:25','K-f1efdj',99000),
    ('TRP-95624','2018-10-27 11:51:15','K-vfvc8s',20000),
    ('TRP-36034','2016-05-04 11:53:39','K-rneg2u',90000),
    ('TRP-83043','2015-07-08 18:26:01','K-vfvc8s',28102),
    ('TRP-73790','2015-09-03 10:53:10','K-j0ztog',100000),
    ('TRP-34129','2020-01-16 19:29:18','K-hk58bb',33000),
    ('TRP-40912','2016-01-26 09:24:05','K-x08117',86000),
    ('TRP-79653','2015-05-20 12:15:01','K-rneg2u',46000),
    ('TRP-51836','2018-03-02 09:02:19','K-3j17ku',177000),
    ('TRP-42540','2017-04-25 17:04:21','K-j3xnl2',60000);


CREATE TABLE TRANSAKSI_DENGAN_RESEP(
    no_resep VARCHAR(20),
    nama_dokter VARCHAR(50) NOT NULL,
    nama_pasien VARCHAR(50) NOT NULL,
    isi_resep TEXT NOT NULL,
    unggahan_resep TEXT,
    tanggal_resep DATE NOT NULL,
    status_validasi VARCHAR(20) NOT NULL,
    no_telepon VARCHAR(20),
    id_transaksi_pembelian VARCHAR(10) NOT NULL,
    email_apoteker VARCHAR(50),
    PRIMARY KEY(no_resep),
    FOREIGN KEY (id_transaksi_pembelian) REFERENCES
    TRANSAKSI_PEMBELIAN (id_transaksi_pembelian),
    FOREIGN KEY (email_apoteker) REFERENCES
    APOTEKER (email)
);

INSERT INTO TRANSAKSI_DENGAN_RESEP VALUES
    (6216319,'Udale Hammerberger','Myles Wreath','Panadol 2 kotak',NULL,'2019-10-05 00:00:00','Valid',3089881887,'TRP-60036','dolifardq@myspace.com'),
    (2834358,'Adaline Servis','Matty Storrie','Ester-C 1 botol',NULL,'2017-09-15 00:00:00','Valid',4064996178,'TRP-09359','lbendar@redcross.org'),
    (1325985,'Erik Morrall','Fran Ramble','Panadol 2 biji',NULL,'2016-03-18 00:00:00','Valid',8336731166,'TRP-12043','jmillions@elegantthemes.com'),
    (4882752,'Minda Tonkin','Bridie O''Grogane','Tidak ada, kamu kena kanker stadium 4',NULL,'2016-01-21 00:00:00','Tidak Valid',9069286421,'TRP-62799','ldaoustt@angelfire.com'),
    (1532048,'Mack Cozens','Holt Corpe','Weed 100 gram',NULL,'2015-08-16 00:00:00','Tidak Valid',4812379023,'TRP-26224','cgaynor1@live.com'),
    (4592436,'Colver Fer','Gaynor Naulty','Sangubion Tablet 3 kotak',NULL,'2017-03-03 00:00:00','Valid',8212906114,'TRP-34129','asrieldreemur@undertale.com'),
    (1857003,'Osborn Shorto','Desiree Ridwood','Neozep tablet 6 biji',NULL,'2020-03-05 00:00:00','Valid',1029418842,'TRP-40912','lbendar@redcross.org'),
    (5990997,'Amii Widdecombe','Davina Bonnett','Antangin 3 sachet',NULL,'2019-07-31 00:00:00','Valid',1567887330,'TRP-79653','jmillions@elegantthemes.com'),
    (1945795,'Timoteo Rapp','Bailie Jeduch','Corrondum 30 gram',NULL,'2016-09-02 00:00:00','Valid',8163966676,'TRP-51836','ldaoustt@angelfire.com'),
    (4822039,'Cesya Lovewell','Blinnie Kornes','Amphetamine 50 gram',NULL,'2015-06-10 00:00:00','Tidak Valid',5573792335,'TRP-42540','lbendar@redcross.org');


CREATE TABLE TRANSAKSI_ONLINE(
    no_urut_pembelian VARCHAR(20),
    id_transaksi_pembelian VARCHAR(10),
    PRIMARY KEY (no_urut_pembelian),
    FOREIGN KEY (id_transaksi_pembelian) REFERENCES
    TRANSAKSI_PEMBELIAN (id_transaksi_pembelian)
);

INSERT INTO TRANSAKSI_ONLINE VALUES
    (12,'TRP-40912'),
    (21,'TRP-69051'),
    (30,'TRP-95624'),
    (79,'TRP-73790'),
    (58,'TRP-34129');


CREATE TABLE PRODUK(
    id_produk VARCHAR(10),
    PRIMARY KEY (id_produk)
);

INSERT INTO PRODUK VALUES
    ('PR-01'),
    ('PR-02'),
    ('PR-03'),
    ('PR-04'),
    ('PR-05'),
    ('PR-06'),
    ('PR-07'),
    ('PR-08'),
    ('PR-09'),
    ('PR-10'),
    ('PR-11'),
    ('PR-12'),
    ('PR-13'),
    ('PR-14'),
    ('PR-15'),
    ('PR-16'),
    ('PR-17'),
    ('PR-18'),
    ('PR-19'),
    ('PR-20'),
    ('PR-21'),
    ('PR-22'),
    ('PR-23'),
    ('PR-24'),
    ('PR-25'),
    ('PR-26'),
    ('PR-27'),
    ('PR-28'),
    ('PR-29'),
    ('PR-30'),
    ('PR-31'),
    ('PR-32'),
    ('PR-33'),
    ('PR-34'),
    ('PR-35'),
    ('PR-36'),
    ('PR-37'),
    ('PR-38'),
    ('PR-39'),
    ('PR-40'),
    ('PR-41'),
    ('PR-42'),
    ('PR-43'),
    ('PR-44'),
    ('PR-45');


CREATE TABLE PRODUK_APOTEK(
    harga_jual INTEGER NOT NULL,
    stok INTEGER NOT NULL,
    satuan_penjualan varchar(5) NOT NULL,
    id_produk VARCHAR(10),
    id_apotek VARCHAR(10),
    PRIMARY KEY (id_produk, id_apotek),
    FOREIGN KEY (id_produk) REFERENCES
    PRODUK (id_produk),
    FOREIGN KEY (id_apotek) REFERENCES
    APOTEK (id_apotek)
);

INSERT INTO PRODUK_APOTEK VALUES
    (5000,99,'Tblt','PR-01','APTK-0005'),
    (24000,55,'Btl','PR-02','APTK-0006'),
    (29000,22,'Scht','PR-03','APTK-0002'),
    (8000,51,'Ktk','PR-04','APTK-0003'),
    (9000,31,'Buah','PR-05','APTK-0002'),
    (5000,25,'Dus','PR-06','APTK-0003'),
    (5000,4,'Tblt','PR-07','APTK-0004'),
    (24000,31,'Scht','PR-08','APTK-0006'),
    (13000,20,'Ktk','PR-09','APTK-0001'),
    (28000,84,'Btl','PR-10','APTK-0005'),
    (13000,36,'Tblt','PR-11','APTK-0003'),
    (32000,84,'Srp','PR-12','APTK-0004'),
    (36000,27,'Tblt','PR-13','APTK-0002'),
    (33000,11,'Btl','PR-14','APTK-0004'),
    (22000,73,'Srp','PR-15','APTK-0005'),
    (31000,41,'Btl','PR-16','APTK-0002'),
    (37000,53,'Scht','PR-17','APTK-0003'),
    (39000,97,'Ktk','PR-18','APTK-0002'),
    (16000,73,'Buah','PR-19','APTK-0005'),
    (11000,73,'Btl','PR-20','APTK-0001'),
    (23000,66,'Tblt','PR-21','APTK-0002'),
    (5000,93,'Srp','PR-22','APTK-0002'),
    (7000,48,'Tblt','PR-23','APTK-0003'),
    (9000,48,'Scht','PR-24','APTK-0004'),
    (34000,35,'Ktk','PR-25','APTK-0006'),
    (26000,70,'Buah','PR-26','APTK-0002'),
    (7000,22,'Dus','PR-27','APTK-0004'),
    (27000,87,'Tblt','PR-28','APTK-0005'),
    (17000,63,'Scht','PR-29','APTK-0001'),
    (35000,85,'Ktk','PR-30','APTK-0002');


CREATE TABLE LIST_PRODUK_DIBELI(
    jumlah INTEGER NOT NULL,
    id_apotek VARCHAR(10),
    id_produk VARCHAR(10),
    id_transaksi_pembelian VARCHAR(10),
    PRIMARY KEY (id_apotek, id_produk, id_transaksi_pembelian),
    FOREIGN KEY (id_produk, id_apotek) REFERENCES
    PRODUK_APOTEK (id_produk, id_apotek),
    FOREIGN KEY (id_transaksi_pembelian) REFERENCES
    TRANSAKSI_PEMBELIAN (id_transaksi_pembelian)
);

INSERT INTO LIST_PRODUK_DIBELI VALUES
    (15,'APTK-0005','PR-01','TRP-60036'),
    (3,'APTK-0006','PR-02','TRP-09359'),
    (4,'APTK-0002','PR-03','TRP-12043'),
    (9,'APTK-0003','PR-04','TRP-62799'),
    (10,'APTK-0002','PR-05','TRP-26224'),
    (7,'APTK-0003','PR-06','TRP-69051'),
    (7,'APTK-0004','PR-07','TRP-95624'),
    (3,'APTK-0006','PR-08','TRP-36034'),
    (12,'APTK-0001','PR-09','TRP-26224'),
    (14,'APTK-0005','PR-10','TRP-69051'),
    (1,'APTK-0003','PR-11','TRP-95624'),
    (9,'APTK-0004','PR-12','TRP-79653'),
    (9,'APTK-0002','PR-13','TRP-51836'),
    (8,'APTK-0004','PR-14','TRP-42540'),
    (9,'APTK-0005','PR-15','TRP-83043'),
    (14,'APTK-0002','PR-16','TRP-73790'),
    (14,'APTK-0003','PR-17','TRP-34129'),
    (10,'APTK-0002','PR-18','TRP-40912'),
    (5,'APTK-0005','PR-19','TRP-26224'),
    (7,'APTK-0001','PR-20','TRP-69051'),
    (6,'APTK-0002','PR-21','TRP-95624'),
    (2,'APTK-0002','PR-22','TRP-36034'),
    (11,'APTK-0003','PR-23','TRP-34129'),
    (11,'APTK-0004','PR-24','TRP-40912'),
    (5,'APTK-0006','PR-25','TRP-79653'),
    (9,'APTK-0002','PR-26','TRP-51836'),
    (5,'APTK-0004','PR-27','TRP-42540'),
    (7,'APTK-0005','PR-28','TRP-42540'),
    (2,'APTK-0001','PR-29','TRP-83043'),
    (10,'APTK-0002','PR-30','TRP-73790');


CREATE TABLE ALAT_MEDIS(
    id_alat_medis VARCHAR(10),
    nama_alat_medis VARCHAR(50) NOT NULL,
    deskripsi_alat TEXT,
    jenis_penggunaan VARCHAR(20) NOT NULL,
    id_produk VARCHAR(10) NOT NULL,
    PRIMARY KEY (id_alat_medis),
    FOREIGN KEY (id_produk) REFERENCES
    PRODUK (id_produk)
);

INSERT INTO ALAT_MEDIS VALUES
    ('AM-01','stetoskop','alat untuk mendengar detak jantung','berkali kali','PR-31'),
    ('AM-02','termometer','alat untuk melihat suhu tubuh','berkali kali','PR-32'),
    ('AM-03','jarum suntik','untuk ditusuk','sekali','PR-33'),
    ('AM-04','inhaler','untuk penyakit asma','berkali kali','PR-34'),
    ('AM-05','epipen','alat anti alergi','berkali kali','PR-35'),
    ('AM-06','tissue','buat lap ('' v'')','sekali','PR-36'),
    ('AM-07','masker','mencegah debu, virus, dll','sekali','PR-37'),
    ('AM-08','hand sanitizer','membunuh bakteri 99%','berkali kali','PR-38'),
    ('AM-09','sarung tangan','menjaga tangan dari marabahaya','sekali','PR-39'),
    ('AM-10','ph indicator','hitung ph darah','berkali kali','PR-40'),
    ('AM-11','blood test','tes kesehatan darah','sekali','PR-41'),
    ('AM-12','ventilator','untuk sesak nafas','berkali kali','PR-42'),
    ('AM-13','tabung oksigen','untuk sesak nafas','sekali','PR-43'),
    ('AM-14','swab test','tes kamu kena corona atau nggak','berkali kali','PR-44'),
    ('AM-15','alat dialisis','buat gagal ginjal','berkali kali','PR-45');


CREATE TABLE MERK_DAGANG(
    id_merk VARCHAR(10),
    nama_merk VARCHAR(50) NOT NULL,
    perusahaan VARCHAR(50) NOT NULL,
    PRIMARY KEY (id_merk)
);

INSERT INTO MERK_DAGANG VALUES
    ('MR-01','tolak angin','sidomuncul'),
    ('MR-02','realitu','prometheus lab'),
    ('MR-03','indomie','indofood'),
    ('MR-04','ugo','ugo'),
    ('MR-05','ice menthol','djarum'),
    ('MR-06','endcorona','endcorona'),
    ('MR-07','dnal','ern'),
    ('MR-08','sodales','nulla'),
    ('MR-09','libero','mauris'),
    ('MR-10','posuere','vel');


CREATE TABLE MERK_DAGANG_ALAT_MEDIS(
    id_alat_medis VARCHAR(10),
    id_merk VARCHAR(10),
    fitur TEXT,
    dimensi VARCHAR(10) NOT NULL,
    cara_penggunaan TEXT,
    model VARCHAR(20),
    PRIMARY KEY (id_alat_medis, id_merk, fitur),
    FOREIGN KEY (id_alat_medis) REFERENCES
    ALAT_MEDIS (id_alat_medis),
    FOREIGN KEY (id_merk) REFERENCES
    MERK_DAGANG(id_merk)
);

INSERT INTO MERK_DAGANG_ALAT_MEDIS VALUES
    ('AM-01','MR-01','mendengar detak jantung dengan baik','0.5 m','pake ketelinga lalu tempel ke dada','donec'),
    ('AM-02','MR-02','dalam derajat celcius','10x2x3 cm','masukkan ke mulut','donec'),
    ('AM-03','MR-03','sakit sedikit','1 mm','untuk ditusuk','lobortis'),
    ('AM-04','MR-04','klik tombol untuk dihirup','10x2x3 cm','untuk penyakit asma','vel'),
    ('AM-05','MR-05','klik tombol untuk dihirup','10x2x3 cm','alat anti alergi','aliquet'),
    ('AM-06','MR-06','lembut dan menyerap keringat','10x2x3 cm','buat lap ('' v'')','tincidunt'),
    ('AM-07','MR-07','mencegah bakteri dan partikel kecil','10x2x3 cm','mencegah debu, virus, dll','nulla nunc'),
    ('AM-08','MR-08','alkohol 100%','10x2x3 cm','membunuh bakteri 99%','dui'),
    ('AM-09','MR-09','lembut dan menyerap keringat','4x2x19 cm','menjaga tangan dari marabahaya','natoque'),
    ('AM-10','MR-10','mirip kertas lakmus','2cm','hitung ph darah','posuere'),
    ('AM-11','MR-01','bukan buat covid 19','12 cm','tes kesehatan darah','blandit lacinia'),
    ('AM-12','MR-02','buat pasien covid 19','4x3m','untuk sesak nafas','ipsum dolor'),
    ('AM-13','MR-03','buat yang sesak nafas','2m','untuk sesak nafas','hac'),
    ('AM-14','MR-04','makanya dirumah aja','2m','tes kamu kena corona atau nggak','congue'),
    ('AM-15','MR-05','membersihkan darah','1x4x5 m','buat gagal ginjal','mauris');


CREATE TABLE MERK_OBAT(
    id_merk_obat VARCHAR(10),
    golongan VARCHAR(25) NOT NULL,
    nama_dagang VARCHAR(50) NOT NULL,
    perusahaan VARCHAR(50) NOT NULL,
    PRIMARY KEY (id_merk_obat)
);

INSERT INTO MERK_OBAT VALUES
    ('MBT-157','Diuretik','Topamax','Nature''s Way Holding Co.'),
    ('MBT-428','Antidiabetes','Breakout Control','Parfums Christian Dior'),
    ('MBT-646','Antibiotik','DG Health Cold and Flu Relief','The Kroger Co.'),
    ('MBT-850','Antiinflamasi nonsteroid','Gentamicin Sulfate','Kremers Urban Pharmaceuticals Inc.'),
    ('MBT-705','Obat jantung','ONDANSETRON HYDROCHLORIDE','Cardinal Health');


CREATE TABLE OBAT(
    id_obat VARCHAR(10),
    id_produk VARCHAR(10) NOT NULL,
    id_merk_obat VARCHAR(10) NOT NULL,
    netto VARCHAR(10) NOT NULL,
    dosis TEXT NOT NULL,
    aturan_pakai TEXT,
    kontraindikasi TEXT,
    bentuk_kesediaan TEXT NOT NULL,
    PRIMARY KEY (id_obat),
    FOREIGN KEY (id_produk) REFERENCES
    PRODUK (id_produk),
    FOREIGN KEY (id_merk_obat) REFERENCES
    MERK_OBAT (id_merk_obat)
);

INSERT INTO OBAT VALUES
    ('42254-073','PR-30','MBT-157','830 g','2 kapsul, 1 kali sehari',NULL,NULL,'Kapsul'),
    ('57337-060','PR-06','MBT-428','562 g','3 kaplet, 4 kali sehari','Dikonsumsi sehabis makan.',NULL,'Kaplet (kapsul tablet)'),
    ('64543-044','PR-04','MBT-428','556 g','5 pil, 4 kali sehari',NULL,'Tidak boleh diberikan kepada orang yang memiliki diabetes','Pil'),
    ('0904-6288','PR-08','MBT-646','725 g','7 pil, 7 kali dalam tiga hari',NULL,NULL,'Pil'),
    ('61919-140','PR-24','MBT-646','419 g','1 suspensi, 1 kali sehari','Larutkan dalam air. Diminum sebelum tidur.',NULL,'Suspensi'),
    ('68968-6610','PR-15','MBT-850','577 g','8 pil, 1 kali sehari','Dikonsumsi sehabis makan.','Tidak boleh diberikan kepada orang yang memiliki tekanan darah tinggi','Pil'),
    ('54371-471','PR-19','MBT-705','321 g','4 pil, 5 kali sehari',NULL,NULL,'Pil'),
    ('61748-017','PR-14','MBT-850','310 g','1 kaplet, 1 kali sehari',NULL,NULL,'Kaplet (kapsul tablet)'),
    ('11523-1314','PR-14','MBT-428','500 ml','1 sendok, 2 kali sehari','Dikocok terlebih dahulu sebelum dikonsumsi.',NULL,'Larutan'),
    ('63739-644','PR-18','MBT-850','154 g','1/2 kaplet, 5 kali sehari','Dikonsumsi sehabis makan.',NULL,'Kaplet (kapsul tablet)'),
    ('52554-2001','PR-28','MBT-428','120 g','200 mg, 1 kali sehari',NULL,NULL,'Serbuk'),
    ('49351-101','PR-06','MBT-646','90 g','300 mg, 2 kali sehari',NULL,'Tidak boleh diberikan kepada orang yang memiliki asma','Serbuk'),
    ('61727-319','PR-14','MBT-646','580 ml','3 sendok, 1 kali sehari',NULL,NULL,'Larutan'),
    ('55319-132','PR-03','MBT-850','256 g','160 mg, 2 kali sehari',NULL,NULL,'Serbuk'),
    ('49781-036','PR-02','MBT-646','300 ml','1/2 sendok, 3 kali sehari','Dikocok terlebih dahulu sebelum dikonsumsi.',NULL,'Larutan');


CREATE TABLE KATEGORI_OBAT(
    id_kategori VARCHAR(10),
    nama_kategori VARCHAR(50) NOT NULL UNIQUE,
    deskripsi_kategori TEXT,
    PRIMARY KEY (id_kategori)
);

INSERT INTO KATEGORI_OBAT VALUES
    ('KBT-767','Obat resep','Obat yang harus digunakan dengan resep dokter'),
    ('KBT-870','Kosmetika 2',NULL),
    ('KBT-840','Obat bebas','Obat yang boleh digunakan tanpa resep dokter (disebut obat OTC = Over The Counter)'),
    ('KBT-990','Obat psikotropika','Zat/obat yang dapat menurunkan aktivitas otak atau merangsang susunan syaraf pusat dan menimbulkan kelainan perilaku, disertai dengan timbulnya halusinasi (mengkhayal), ilusi, gangguan cara berpikir, perubahan alam perasaan dan dapat menyebabkan ketergantungan serta mempunyai efek stimulasi (merangsang) bagi para pemakainya.'),
    ('KBT-873','Jamu','Obat tradisional yang disediakan secara tradisional, misalnya dalam bentuk serbuk seduhan, pil, dan cairan yang berisi seluruh bahan tanaman yang menjadi penyusun jamu tersebut serta digunakan secara tradisional'),
    ('KBT-121','Obat Herbal Terstandar','Obat tradisional yang disajikan dari ekstrak atau penyarian bahan alam yang dapat berupa tanaman obat, binatang, maupun mineral.'),
    ('KBT-961','Fitofarmaka','Obat tradisional dari bahan alam yang dapat disejajarkan dengan obat modern karena proses pembuatannya yang telah terstandar, ditunjang dengan bukti ilmiah sampai dengan uji klinik pada manusia.'),
    ('KBT-061','Obat generik','Obat dengan nama generik, nama resmi yang telah ditetapkan dalam Farmakope Indonesia dan INN (International Non-propietary Names) dari WHO (World Health Organization) untuk zat berkhasiat yang dikandungnya.'),
    ('KBT-239','Obat nama dagang','Nama sediaan obat yang diberikan oleh pabriknya dan terdaftar di departemen kesehatan suatu negara, disebut juga sebagai merek terdaftar.'),
    ('KBT-860','Kosmetika 1','Kosmetik yang digunakan untuk bayi');


CREATE TABLE KATEGORI_MERK_OBAT(
    id_kategori VARCHAR(10),
    id_merk_obat VARCHAR(10),
    PRIMARY KEY (id_kategori, id_merk_obat),
    FOREIGN KEY (id_kategori) REFERENCES
    KATEGORI_OBAT (id_kategori),
    FOREIGN KEY (id_merk_obat) REFERENCES
    MERK_OBAT (id_merk_obat)
);

INSERT INTO KATEGORI_MERK_OBAT VALUES
    ('KBT-767','MBT-157'),
    ('KBT-870','MBT-428'),
    ('KBT-840','MBT-646'),
    ('KBT-990','MBT-850'),
    ('KBT-873','MBT-705'),
    ('KBT-121','MBT-157'),
    ('KBT-961','MBT-428'),
    ('KBT-061','MBT-646'),
    ('KBT-239','MBT-850'),
    ('KBT-860','MBT-705');


CREATE TABLE KOMPOSISI(
    id_bahan VARCHAR(10),
    nama_bahan VARCHAR(50) NOT NULL UNIQUE,
    PRIMARY KEY (id_bahan)
);

INSERT INTO KOMPOSISI VALUES
    ('COMP-318','Minoxidil'),
    ('COMP-036','Gabapentin'),
    ('COMP-125','ESCITALOPRAM OXALATE'),
    ('COMP-063','STRYCHNOS NUX-VOMICA SEED'),
    ('COMP-937','Oxacillin');


CREATE TABLE KANDUNGAN(
    id_bahan VARCHAR(10),
    id_obat VARCHAR(10),
    takaran VARCHAR(10),
    satuan_takar VARCHAR(10),
    PRIMARY KEY (id_bahan, id_obat, takaran),
    FOREIGN KEY (id_bahan) REFERENCES
    KOMPOSISI (id_bahan),
    FOREIGN KEY (id_obat) REFERENCES
    OBAT (id_obat)
);

INSERT INTO KANDUNGAN VALUES
    ('COMP-937','49351-101','pipet','cc'),
    ('COMP-937','11523-1314','cawan','ml'),
    ('COMP-063','42254-073','sendok','cc'),
    ('COMP-125','63739-644','pipet','cc'),
    ('COMP-125','68968-6610','cawan','ml'),
    ('COMP-125','64543-044','pipet','cc'),
    ('COMP-937','61919-140','puyer','mg'),
    ('COMP-125','68968-6610','pipet','cc'),
    ('COMP-036','57337-060','puyer','mg'),
    ('COMP-318','52554-2001','cawan','ml'),
    ('COMP-937','54371-471','puyer','mg'),
    ('COMP-318','63739-644','pipet','cc'),
    ('COMP-318','63739-644','sendok','cc'),
    ('COMP-318','52554-2001','sendok','cc'),
    ('COMP-063','49351-101','cawan','ml'),
    ('COMP-125','64543-044','puyer','cc'),
    ('COMP-036','54371-471','sendok','cc'),
    ('COMP-937','61748-017','pipet','cc'),
    ('COMP-318','64543-044','puyer','mg'),
    ('COMP-063','57337-060','sendok','cc');


CREATE TABLE BALAI_PENGOBATAN(
    id_balai VARCHAR(10),
    alamat_balai TEXT NOT NULL UNIQUE,
    nama_balai VARCHAR(50) NOT NULL,
    jenis_balai VARCHAR(30) NOT NULL,
    telepon_balai VARCHAR(20),
    PRIMARY KEY (id_balai)
);

INSERT INTO BALAI_PENGOBATAN VALUES
    ('BL-JQ228','8354 Hauk Terrace','Hills, Marks and Jacobi','THT','974 460 0647'),
    ('BL-OA390','5438 Oxford Plaza','Kutch, O''Hara and Greenholt','Umum','327 603 8410'),
    ('BL-JU773','12009 Muir Park','Roberts-Howell','THT','853 356 2900'),
    ('BL-UG514','34639 Swallow Court','Bode-Kuhic','Gigi','676 847 9804'),
    ('BL-QC950','02274 Blackbird Point','Johns Inc','Umum','167 105 7896');


CREATE TABLE BALAI_APOTEK(
    id_balai VARCHAR(10),
    id_apotek VARCHAR(10),
    PRIMARY KEY (id_balai, id_apotek),
    FOREIGN KEY (id_balai) REFERENCES
    BALAI_PENGOBATAN (id_balai),
    FOREIGN KEY (id_apotek) REFERENCES
    APOTEK (id_apotek)
);

INSERT INTO BALAI_APOTEK VALUES
    ('BL-JQ228','APTK-0001'),
    ('BL-OA390','APTK-0002'),
    ('BL-JU773','APTK-0003'),
    ('BL-UG514','APTK-0004'),
    ('BL-QC950','APTK-0005');


CREATE TABLE PENGANTARAN_FARMASI(
    id_pengantaran VARCHAR(10),
    id_kurir VARCHAR(10) NOT NULL,
    id_transaksi_pembelian VARCHAR(10),
    waktu TIMESTAMP NOT NULL,
    status_pengantaran VARCHAR(20) NOT NULL,
    biaya_kirim INTEGER NOT NULL,
    total_biaya INTEGER NOT NULL,
    PRIMARY KEY (id_pengantaran),
    FOREIGN KEY (id_kurir) REFERENCES
    KURIR (id_kurir),
    FOREIGN KEY (id_transaksi_pembelian)
    REFERENCES TRANSAKSI_PEMBELIAN (id_transaksi_pembelian)
);

INSERT INTO PENGANTARAN_FARMASI VALUES
    ('PFM-484','KR-IR572','TRP-42540','2019-12-06 20:10:34','DELIVERED',57000,490000),
    ('PFM-833','KR-LU226','TRP-62799','2020-02-14 16:29:28','ON THE WAY',96000,437000),
    ('PFM-925','KR-HC124','TRP-62799','2019-05-07 09:37:54','DELIVERED',71000,95000),
    ('PFM-683','KR-IR572','TRP-79653','2020-01-25 23:52:23','ON THE WAY',84000,11000),
    ('PFM-473','KR-IR572','TRP-79653','2018-10-23 14:13:44','ON THE WAY',6000,79000),
    ('PFM-734','KR-LX330','TRP-12043','2019-02-22 20:40:28','READY',73000,718000),
    ('PFM-914','KR-LX330','TRP-12043','2017-11-20 04:22:15','ON THE WAY',8000,361000),
    ('PFM-698','KR-IR572','TRP-83043','2018-10-05 09:27:07','DELIVERED',43000,272000);

---
--- Alter all foreign key dependent to APOTEK and PRODUK APOTEK to be CASCADE
--- 

ALTER TABLE list_produk_dibeli
DROP CONSTRAINT list_produk_dibeli_id_produk_id_apotek_fkey; 

ALTER TABLE list_produk_dibeli
ADD CONSTRAINT list_produk_dibeli_id_produk_id_apotek_fkey
FOREIGN KEY (id_produk, id_apotek) 
REFERENCES PRODUK_APOTEK(id_produk, id_apotek) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE list_produk_dibeli 
DROP CONSTRAINT list_produk_dibeli_id_transaksi_pembelian_fkey;  

ALTER TABLE list_produk_dibeli
ADD CONSTRAINT list_produk_dibeli_id_transaksi_pembelian_fkey
FOREIGN KEY (id_transaksi_pembelian) 
REFERENCES TRANSAKSI_PEMBELIAN(id_transaksi_pembelian) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE admin_apotek 
DROP CONSTRAINT admin_apotek_id_apotek_fkey;  

ALTER TABLE admin_apotek
ADD CONSTRAINT admin_apotek_id_apotek_fkey
FOREIGN KEY (id_apotek) REFERENCES APOTEK(id_apotek) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE produk_apotek 
DROP CONSTRAINT produk_apotek_id_apotek_fkey;  

ALTER TABLE produk_apotek
ADD CONSTRAINT produk_apotek_id_apotek_fkey
FOREIGN KEY (id_apotek) REFERENCES APOTEK(id_apotek) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE produk_apotek 
DROP CONSTRAINT produk_apotek_id_produk_fkey;  

ALTER TABLE produk_apotek
ADD CONSTRAINT produk_apotek_id_produk_fkey
FOREIGN KEY (id_produk) REFERENCES PRODUK(id_produk) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE balai_apotek 
DROP CONSTRAINT balai_apotek_id_apotek_fkey;  

ALTER TABLE balai_apotek
ADD CONSTRAINT balai_apotek_id_apotek_fkey
FOREIGN KEY(id_apotek) REFERENCES APOTEK(id_apotek)
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE transaksi_dengan_resep
DROP CONSTRAINT transaksi_dengan_resep_id_transaksi_pembelian_fkey;
ALTER TABLE transaksi_dengan_resep
ADD CONSTRAINT transaksi_dengan_resep_id_transaksi_pembelian_fkey
FOREIGN KEY(id_transaksi_pembelian) REFERENCES TRANSAKSI_PEMBELIAN(id_transaksi_pembelian)
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE PENGANTARAN_FARMASI
DROP CONSTRAINT pengantaran_farmasi_id_transaksi_pembelian_fkey;
ALTER TABLE PENGANTARAN_FARMASI
ADD CONSTRAINT pengantaran_farmasi_id_transaksi_pembelian_fkey
FOREIGN KEY(id_transaksi_pembelian) REFERENCES TRANSAKSI_PEMBELIAN(id_transaksi_pembelian)
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE transaksi_online
DROP CONSTRAINT transaksi_online_id_transaksi_pembelian_fkey;
ALTER TABLE transaksi_online
ADD CONSTRAINT transaksi_online_id_transaksi_pembelian_fkey
FOREIGN KEY(id_transaksi_pembelian) REFERENCES TRANSAKSI_PEMBELIAN(id_transaksi_pembelian)
ON UPDATE CASCADE ON DELETE CASCADE;

---
--- Alter all foreign key dependent to BALAI_PENGOBATAN to be CASCADE
--- 

ALTER TABLE balai_apotek
DROP CONSTRAINT balai_apotek_id_balai_fkey; 

ALTER TABLE balai_apotek
ADD CONSTRAINT balai_apotek_id_balai_fkey
FOREIGN KEY (id_balai) 
REFERENCES BALAI_PENGOBATAN(id_balai) 
ON UPDATE CASCADE ON DELETE CASCADE;

---
--- Dummy accounts for User Testing
--- 

INSERT INTO PENGGUNA VALUES
    ('admin@farmakami.com',08123456789,'kel31','Admin Apotek Kece','admin'),
    ('cs@farmakami.com',08123456788,'kel31','Customer Service Less Kece','cs'),
    ('konsumen@farmakami.com',08123456787,'kel31','Konsumen Beli Beli','konsumen'),
    ('kurir@farmakami.com',08123456786,'kel31','Kurir Antar Antar','kurir');

INSERT INTO KONSUMEN VALUES
    ('K-000000', 'konsumen@farmakami.com', 'M', '1999-01-01');

INSERT INTO KURIR VALUES
    ('KR-AA000', 'kurir@farmakami.com', 'Test Kurir');

INSERT INTO APOTEKER VALUES
    ('admin@farmakami.com'),
    ('cs@farmakami.com');

INSERT INTO ADMIN_APOTEK VALUES
    ('admin@farmakami.com', 'APTK-0001');

INSERT INTO CS VALUES
    ('00000000000000000000', 'cs@farmakami.com', '000000000000');
---
--- Delete incorrect trigger answer keys
--- 

-- DROP TRIGGER trigger_calculate_stok ON LIST_PRODUK_DIBELI;
-- DROP FUNCTION calculate_stok;
-- DROP TRIGGER trigger_calculate_total_pembayaran ON LIST_PRODUK_DIBELI;
-- DROP FUNCTION calculate_total_pembayaran;
-- DROP TRIGGER trigger_calculate_total_biaya ON PENGANTARAN_FARMASI;
-- DROP FUNCTION calculate_total_biaya;
-- DROP TRIGGER trigger_update_status_pengiriman ON PENGANTARAN_FARMASI;
-- DROP FUNCTION update_status_pengantaran;

---
--- Add stored procedure and trigger 1
--- 

CREATE OR REPLACE FUNCTION UPDATE_STOK()
RETURNS TRIGGER as $$
DECLARE flag BOOLEAN;
DECLARE temp INTEGER;
BEGIN
	flag := (SELECT stok from PRODUK_APOTEK where id_produk = NEW.id_produk and id_apotek = NEW.id_apotek) > NEW.jumlah;
	IF (flag = true) THEN
		UPDATE PRODUK_APOTEK set stok = stok - NEW.jumlah
		WHERE id_produk = NEW.id_produk and id_apotek = NEW.id_apotek;
	ELSE
		temp := (SELECT stok from PRODUK_APOTEK where id_produk = NEW.id_produk and id_apotek = NEW.id_apotek);
		UPDATE LIST_PRODUK_DIBELI set jumlah = temp
		where id_produk = NEW.id_produk and id_apotek = NEW.id_apotek;
	END IF; 
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER UPDATE_STOK_LIST
AFTER INSERT on LIST_PRODUK_DIBELI
FOR EACH ROW
EXECUTE PROCEDURE UPDATE_STOK();

---
--- Add stored procedure and trigger 2
--- 

CREATE OR REPLACE FUNCTION calculate_total_pembayaran()
RETURNS trigger As
	$$
	DECLARE
	harga_jual_soal Integer;
	total_pembayaran_soal Integer;
	BEGIN
		Select harga_jual into harga_jual_soal
		From PRODUK_APOTEK PA
		Where New.id_apotek = PA.id_apotek
		AND New.id_produk = PA.id_produk;
		total_pembayaran_soal := harga_jual_soal * New.jumlah;

		UPDATE TRANSAKSI_PEMBELIAN
		SET total_pembayaran = total_pembayaran_soal
		WHERE id_transaksi_pembelian = New.id_transaksi_pembelian;
		RETURN NEW;
	END;
	$$
LANGUAGE plpgsql;


CREATE TRIGGER trigger_calculate_total_pembayaran
AFTER INSERT ON LIST_PRODUK_DIBELI
FOR EACH ROW
EXECUTE PROCEDURE calculate_total_pembayaran();

---
--- Add stored procedure and trigger 3
--- 

CREATE OR REPLACE FUNCTION jumlah_total_biaya()
RETURNS TRIGGER AS
$$
DECLARE biaya_kirim_soal INTEGER;
DECLARE total_pembayaran_soal INTEGER;
BEGIN
    SELECT total_pembayaran INTO total_pembayaran_soal FROM TRANSAKSI_PEMBELIAN
    WHERE id_transaksi_pembelian = NEW.id_transaksi_pembelian;
    biaya_kirim_soal := NEW.biaya_kirim;

    UPDATE PENGANTARAN_FARMASI
    SET total_biaya = biaya_kirim_soal + total_pembayaran_soal
    WHERE id_pengantaran = NEW.id_pengantaran;
    RETURN NEW;
END;

$$
LANGUAGE plpgsql;

CREATE TRIGGER trigger_jumlah_biaya
AFTER INSERT on PENGANTARAN_FARMASI
FOR EACH ROW
EXECUTE PROCEDURE jumlah_total_biaya();

---
--- Add stored procedure and trigger 5
--- 

CREATE OR REPLACE FUNCTION update_status_pengantaran()
	RETURNS trigger AS
	$$
		DECLARE
		-- set id kurir default
		id_kurir_default Integer := 1;

		BEGIN
			IF (id_kurir_default <> NEW.id_kurir) THEN
				IF (OLD.status_pengantaran <> 'DONE' AND OLD.status_pengantaran <> 'Processing') THEN
					UPDATE PENGANTARAN_FARMASI
					SET NEW.status_pengantaran = 'Processing';
				END IF;
			END IF;
			RETURN NEW;
		END;
	$$
LANGUAGE plpgsql;

CREATE TRIGGER trigger_update_status_pengiriman
BEFORE UPDATE of id_kurir ON PENGANTARAN_FARMASI
FOR EACH ROW
EXECUTE PROCEDURE update_status_pengantaran();
