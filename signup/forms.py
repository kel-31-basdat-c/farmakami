from django import forms

GENDER = [
    ('M','Laki-laki'),
    ('F','Perempuan')]

date_attrs = {'class': 'inline-flex third-width'}

class AdminRoleForm(forms.Form):
    email = forms.CharField(label = 'Email', max_length=50, 
        widget=forms.EmailInput(attrs={ 'placeholder': 'farmakami@gmail.com' }))
    password = forms.CharField(label = 'Password',  max_length=128, widget=forms.PasswordInput)
    nama_lengkap = forms.CharField(label = 'Nama Lengkap', max_length=50,
        widget=forms.TextInput(attrs={'placeholder': 'Joe Piplupus'}))
    no_telp = forms.CharField(label = 'No Telepon', max_length=20,
        widget=forms.TextInput(attrs={'placeholder': '+62 876 23232323'}))

class KonsumenRoleForm(forms.Form):
    email = forms.CharField(label = 'Email', max_length=50, 
        widget=forms.EmailInput(attrs={ 'placeholder': 'farmakami@gmail.com' }))
    password = forms.CharField(label = 'Password',  max_length=128, widget=forms.PasswordInput)
    nama_lengkap = forms.CharField(label = 'Nama Lengkap', max_length=50,
        widget=forms.TextInput(attrs={'placeholder': 'Joe Piplupus'}))
    no_telp = forms.CharField(label = 'No Telepon', max_length=20, 
        widget=forms.TextInput(attrs={'placeholder': '+62 876 23232323'}))
    jenis_kelamin = forms.ChoiceField(label = 'Jenis Kelamin', choices=GENDER, widget=forms.Select)
    date = forms.DateField(label = 'Tanggal Lahir', 
        widget=forms.SelectDateWidget(attrs=date_attrs, years=range(1945, 2005)), )

class KurirRoleForm(forms.Form):
    email = forms.CharField(label = 'Email', max_length=50, 
        widget=forms.EmailInput(attrs={ 'placeholder': 'farmakami@gmail.com' }))
    password = forms.CharField(label = 'Password',  max_length=128, widget=forms.PasswordInput)
    nama_lengkap = forms.CharField(label = 'Nama Lengkap', max_length=50,
        widget=forms.TextInput(attrs={'placeholder': 'Joe Piplupus'}))
    no_telp = forms.CharField(label = 'No Telepon', max_length=20,
        widget=forms.TextInput(attrs={'placeholder': '+62 876 23232323'}))
    nama_perusahaan = forms.CharField(label = 'Nama Perusahaan', max_length=50,
        widget=forms.TextInput(attrs={'placeholder': 'PT Farmakami Basdat'}))

class CSRoleForm(forms.Form):
    email = forms.CharField(label = 'Email', max_length=50, 
        widget=forms.EmailInput(attrs={ 'placeholder': 'farmakami@gmail.com' }))
    password = forms.CharField(label = 'Password',  max_length=128, widget=forms.PasswordInput)
    nama_lengkap = forms.CharField(label = 'Nama Lengkap', max_length=50,
        widget=forms.TextInput(attrs={'placeholder': 'Joe Piplupus'}))
    no_telp = forms.CharField(label = 'No Telepon', max_length=20,
        widget=forms.TextInput(attrs={'placeholder': '+62 876 23232323'}))
    no_ktp = forms.CharField(label = 'No KTP', max_length=20,
        widget=forms.TextInput(attrs={'placeholder': '78719191919'}))
    no_sia = forms.CharField(label = 'Nomor SIA', max_length=20,
        widget=forms.TextInput(attrs={'placeholder': '1718912345'}))
    