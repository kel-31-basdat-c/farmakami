from django.urls import path
from . import views

app_name = 'signup'

urlpatterns = [
    path('konsumen', views.insertKonsumen, name='konsumen'),
    path('kurir', views.insertKurir, name='kurir'),
    path('admin', views.insertAdmin, name='admin'),
    path('cs', views.insertCS, name='cs'),
]
