from django.shortcuts import render, redirect
from .forms import AdminRoleForm, KonsumenRoleForm, KurirRoleForm, CSRoleForm
from django.db import connection
from django.contrib import messages
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def insertAdmin(request):
    context = {}
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        nama_lengkap = request.POST['nama_lengkap']
        no_telp = request.POST['no_telp']

        cursor = connection.cursor()
        cursor.execute("select * from pengguna where email='"+email+"'")
        hasil = namedtuplefetchall(cursor)
        if (hasil == []):
            cursor.execute("insert into pengguna values ('"+email+"','"+no_telp+"','"+password+"','"+nama_lengkap+"','admin')")
            cursor.execute("insert into apoteker values ('"+email+"')")
            cursor.execute("insert into admin_apotek values ('"+email+"',NULL)")
            cursor.close()
            request.session['auth'] = email
            request.session['role'] = 'admin'
            del request.session['signup']
            messages.success(request, nama_lengkap)
            return redirect('profil:profil')
        else:
            cursor.close()
            context['form'] = AdminRoleForm(request.POST)
            context['error'] = "Email sudah digunakan"
            return render(request, 'signup.html', context)
    else:
        context['form'] = AdminRoleForm
        request.session['signup'] = 'admin'
        return render(request, 'signup.html', context)

def insertKurir(request):
    context = {}
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        nama_lengkap = request.POST['nama_lengkap']
        no_telp = request.POST['no_telp']
        nama_perusahaan = request.POST['nama_perusahaan']

        cursor = connection.cursor()
        cursor.execute("select count(*) as c from kurir")
        jml_kurir = namedtuplefetchall(cursor)
        id_kurir = jml_kurir[0].c

        cursor.execute("select * from pengguna where email='"+email+"'")
        hasil = namedtuplefetchall(cursor)
        if (hasil == []):
            while True:
                idk = str(id_kurir)
                cursor.execute("select id_kurir from kurir where id_kurir='"+idk+"'")
                temp = namedtuplefetchall(cursor)
                if(temp!=[]):
                    id_kurir = id_kurir + 1
                    continue
                else:
                    break
                        
            cursor.execute("insert into pengguna values ('"+email+"','"+no_telp+"','"+password+"','"+nama_lengkap+"','kurir')")
            idk = str(id_kurir)
            cursor.execute("insert into kurir values ('"+idk+"','"+email+"','"+nama_perusahaan+"')")
            cursor.close()
            request.session['auth'] = email
            request.session['role'] = 'kurir'
            del request.session['signup']
            messages.success(request, nama_lengkap)
            return redirect('profil:profil')
        else:
            cursor.close()
            context['form'] = KurirRoleForm(request.POST)
            context['error'] = "Email sudah digunakan"
            return render(request, 'signup.html', context)
    else:
        context['form'] = KurirRoleForm
        request.session['signup'] = 'kurir'
        return render(request, 'signup.html', context)

def insertCS(request):
    context = {}
    if request.method == "POST":
        email = str(request.POST['email'])
        password = str(request.POST['password'])
        nama_lengkap = str(request.POST['nama_lengkap'])
        no_telp = str(request.POST['no_telp'])
        no_ktp = str(request.POST['no_ktp'])
        no_sia = str(request.POST['no_sia'])
        
        cursor = connection.cursor()
        cursor.execute("select * from pengguna where email='"+email+"'")
        hasil = namedtuplefetchall(cursor)
        if (hasil == []):
            cursor.execute("select no_ktp from cs where no_ktp='"+no_ktp+"'")
            hasil = namedtuplefetchall(cursor)
            if (hasil == []):
                cursor.execute("insert into pengguna values ('"+email+"','"+no_telp+"','"+password+"','"+nama_lengkap+"','cs')")
                cursor.execute("insert into apoteker values ('"+email+"')")
                cursor.execute("insert into cs values ('"+no_ktp+"','"+email+"','"+no_sia+"')")
                cursor.close()
                request.session['auth'] = email
                request.session['role'] = 'cs'
                del request.session['signup']
                messages.success(request, nama_lengkap)
                return redirect('profil:profil')
            else:
                cursor.close()
                context['form'] = CSRoleForm(request.POST)
                context['error'] = "No KTP CS sudah terdaftar"
                return render(request, 'signup.html', context)
        else:
            cursor.close()
            context['form'] = CSRoleForm(request.POST)
            context['error'] = "Email sudah digunakan"
            return render(request, 'signup.html', context)
    else:
        context['form'] = CSRoleForm
        request.session['signup'] = 'cs'
        return render(request, 'signup.html', context)
    
def insertKonsumen(request):
    context = {}
    if request.method == "POST":
        form = KonsumenRoleForm(request.POST)
        if not form.is_valid():
            context['form'] = form
            context['error'] = "Tanggal lahir tidak valid"
            return render(request, 'signup.html', context)

        email = str(request.POST['email'])
        password = str(request.POST['password'])
        nama_lengkap = str(request.POST['nama_lengkap'])
        no_telp = request.POST['no_telp']
        jenis_kelamin = request.POST['jenis_kelamin']
        tanggal_lahir = request.POST['date_year'] + "-" + request.POST['date_month'] + "-" + request.POST['date_day']
        alamat = []
        status = []
        count_id = 0

        cursor = connection.cursor()
        cursor.execute("select * from pengguna where email='"+email+"'")
        hasil = namedtuplefetchall(cursor)
        if (hasil != []):
            cursor.close()
            context['form'] = form
            context['error'] = "Email sudah digunakan"
            return render(request, 'signup.html', context)

        while True:
            try:
                count_id = count_id + 1
                id_alamat = "alamat"+ str(count_id)
                hasil1 = str(request.POST[id_alamat])
                alamat.append(hasil1)
                id_status = "status"+ str(count_id)
                hasil2 = str(request.POST[id_status])
                status.append(hasil2)
            except Exception as e:
                break
        
        panjang_alamat = len(alamat)
        for i in range(panjang_alamat):
            for j in range(panjang_alamat):
                if (i!=j):
                    if(alamat[i]==alamat[j] and status[i]==status[j]):
                        cursor.close()
                        context['form'] = form
                        context['error'] = "Alamat dan status tidak boleh duplikat"
                        return render(request, 'signup.html', context)

        cursor.execute("select count(*) as c from konsumen")
        jml_konsumen = namedtuplefetchall(cursor)
        id_konsumen = jml_konsumen[0].c
        while True:
            idk = str(id_konsumen)
            cursor.execute("select id_konsumen from konsumen where id_konsumen='"+idk+"'")
            temp = namedtuplefetchall(cursor)
            if(temp!=[]):
                id_konsumen = id_konsumen + 1
                continue
            else:
                break
    
        id_konsumen = id_konsumen
        idk = str(id_konsumen)

        cursor.execute("insert into pengguna values ('"+email+"','"+no_telp+"','"+password+"','"+nama_lengkap+"','konsumen')")
        cursor.execute("insert into konsumen values ('"+idk+"','"+email+"','"+jenis_kelamin+"','"+tanggal_lahir+"')")
        for i in range(panjang_alamat):
            cursor.execute("insert into alamat_konsumen values ('"+idk+"','"+alamat[i]+"','"+status[i]+"')")
        cursor.close()

        request.session['auth'] = email
        request.session['role'] = 'konsumen'
        del request.session['signup']
        messages.success(request, nama_lengkap)
        return redirect('profil:profil')
    else:
        context['form'] = KonsumenRoleForm
        request.session['signup'] = 'konsumen'
        return render(request, 'signup.html', context)
