# Farmakami

[![pipeline status](https://gitlab.com/kel-31-basdat-c/farmakami/badges/master/pipeline.svg)](https://gitlab.com/kel-31-basdat-c/farmakami/-/commits/master)

## Authors (Kelompok 31)
* [Karin Patricia](https://gitlab.com/karinptrc)
* [Marthin Gabriela](https://gitlab.com/marthin.gabriela)
* [Rendya Yuschak](https://gitlab.com/Haskucy)
* [Wulan Mantiri](https://gitlab.com/wulanmantiri_)

## Acknowledgements
* CS UI - Databases C 2020