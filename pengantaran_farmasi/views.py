from django.shortcuts import render, redirect
from django.db import connection
from .forms import *

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def pengantaran_farmasi(request):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM pengantaran_farmasi')
        pengantaran_farmasi = dictfetchall(cursor)
    return render(request, 'pengantaran-farmasi.html', {'pengantaran_farmasi':pengantaran_farmasi})

def form_pengantaran_farmasi(request):
    context = {}
    if request.method == 'POST':
        form = PengantaranFarmasiForm(request.POST)
        form.fields['id_pengantaran'].widget = forms.HiddenInput()
        form.fields['id_kurir'].widget = forms.HiddenInput()
        form.fields['id_transaksi_pembelian'].widget = forms.HiddenInput()
        form.fields['total_biaya'].widget = forms.HiddenInput()
        context['form'] = form
        context['id_pengantaran'] = request.POST['id_pengantaran']
        context['id_kurir'] = request.POST['id_kurir']
        context['id_transaksi_pembelian'] = request.POST['id_transaksi_pembelian']
        context['waktu'] = request.POST['waktu']
        context['status_pengantaran'] = request.POST['status_pengantaran']
        context['biaya_kirim'] = request.POST['biaya_kirim']
        context['total_biaya'] = request.POST['total_biaya']
        context['action'] = 'Update'

    else:
        context['form'] = PengantaranFarmasiForm()
        context['action'] = 'Create'
    return render(request, 'form-pengantaran-farmasi.html', context)

def register_pengantaran_farmasi(request):
    if request.method == 'POST':
        context = { 'form': PengantaranFarmasiForm(request.POST) }
        id_pengantaran = request.POST['id_pengantaran']
        id_kurir = request.POST['id_kurir']
        id_transaksi_pembelian = request.POST['id_transaksi_pembelian']
        waktu = request.POST['waktu']
        status_pengantaran = request.POST['status_pengantaran']
        biaya_kirim = request.POST['biaya_kirim']
        total_biaya = request.POST['total_biaya']

        # check if primary key exists => error
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM pengantaran_farmasi WHERE NOT (id_pengantaran = %s)',
            [id_pengantaran])
            row = cursor.fetchone()

        if request.POST['action'] == 'Update':
            with connection.cursor() as cursor:
                cursor.execute('UPDATE pengantaran_farmasi SET ' \
                    + 'id_pengantaran = %s, ' \
                    + 'id_transaksi_pembelian = %s, ' \
                    + 'status_pengantaran = %s, biaya_kirim = %s, total_biaya = %s' \
                    + 'WHERE id_pengantaran = %s', \
                    [id_pengantaran, id_transaksi_pembelian, status_pengantaran, biaya_kirim, total_biaya, id_pengantaran])
        elif request.POST['action'] == 'Create':
            if row:
                context['error'] = "ID Pengantaran sudah terambil"
                context['action'] = request.POST['action']
                return render(request, 'form-pengantaran-farmasi.html', context)
            with connection.cursor() as cursor:
                cursor.execute('INSERT INTO pengantaran_farmasi VALUES (%s, %s, %s, %s, %s, %s, %s)', \
                    [id_pengantaran, id_kurir, id_transaksi_pembelian, waktu, status_pengantaran, biaya_kirim, total_biaya])
        return redirect('pengantaran_farmasi:list')
    else:
        return redirect('pengantaran_farmasi:form')

def delete_pengantaran_farmasi(request):
    id_pengantaran = request.POST['id_pengantaran']
    id_kurir = request.POST['id_kurir']
    id_transaksi_pembelian = request.POST['id_transaksi_pembelian']
    with connection.cursor() as cursor:
        cursor.execute('DELETE FROM pengantaran_farmasi WHERE id_pengantaran = %s', \
            [id_pengantaran])
    return redirect('pengantaran_farmasi:list')