from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'pengantaran_farmasi'

urlpatterns = [
    path('', views.pengantaran_farmasi, name="list"),
    path('form', views.form_pengantaran_farmasi, name="form"),
    path('register', views.register_pengantaran_farmasi, name="register"),
    path('delete', views.delete_pengantaran_farmasi, name="delete"),
]