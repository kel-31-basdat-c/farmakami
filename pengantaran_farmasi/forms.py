from django import forms
from django.db import connection

pengantaran_choices = []
with connection.cursor() as cursor:
    cursor.execute('SELECT id_pengantaran FROM pengantaran_farmasi')
    row = cursor.fetchall()
for id_pengantaran in row:
    pengantaran_choices.append((id_pengantaran[0], id_pengantaran[0]))

kurir_choices = []
with connection.cursor() as cursor:
    cursor.execute('SELECT id_kurir FROM kurir')
    row = cursor.fetchall()
for id_kurir in row:
    kurir_choices.append((id_kurir[0], id_kurir[0]))

transaksi_choices = []
with connection.cursor() as cursor:
    cursor.execute('SELECT id_transaksi_pembelian FROM transaksi_pembelian')
    row = cursor.fetchall()
for id_transaksi_pembelian in row:
    transaksi_choices.append((id_transaksi_pembelian[0], id_transaksi_pembelian[0]))

class PengantaranFarmasiForm(forms.Form):
    
    id_pengantaran = forms.ChoiceField(label="ID Pengantaran", widget=forms.Select(), 
        choices=pengantaran_choices, initial=pengantaran_choices[0][0])
    id_kurir = forms.ChoiceField(label="ID Kurir", widget=forms.Select(), 
        choices=kurir_choices, initial=kurir_choices[0][0])
    id_transaksi_pembelian = forms.ChoiceField(label="ID Transaksi Pembelian", widget=forms.Select(), 
        choices=transaksi_choices, initial=transaksi_choices[0][0])
    waktu = forms.CharField(label="Waktu", required=True, widget=forms.TextInput(), initial=" ")
    status_pengantaran = forms.CharField(label="Status Pengantaran", required=True, widget = forms.TextInput())
    biaya_kirim = forms.CharField(label="Biaya Kirim", required=True, widget=forms.NumberInput(), initial=0)
    total_biaya = forms.CharField(label="Total Biaya", required=True, widget=forms.NumberInput(), initial=0)
