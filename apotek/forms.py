from django import forms

class ApotekForm(forms.Form):

    nama_apotek = forms.CharField(label="Nama Apotek", required=True, max_length=50,
        widget=forms.TextInput(attrs={'placeholder': 'Kimia Farmasi'}))
    alamat_apotek = forms.CharField(label="Alamat Apotek", required=True,
        widget=forms.Textarea(attrs={'placeholder': 'Jl. Sucipto No. 9'}))
    telepon_apotek = forms.CharField(label="No Telp Apotek", max_length=20, 
        widget=forms.TextInput(attrs={'placeholder': '+62 876 23232323'}))
    nama_penyelenggara = forms.CharField(label="Nama Penyelenggara", required=True, max_length=50, 
        widget=forms.TextInput(attrs={'placeholder': 'Ahmad Darwin'}))
    no_sia = forms.CharField(label="No SIA Penyelenggara", required=True, max_length=20, 
        widget=forms.TextInput(attrs={'placeholder': '838787236'}))
    email = forms.CharField(label="Email Penyelenggara", required=True, max_length=50, 
        widget=forms.EmailInput(attrs={'placeholder': 'ahmad@email.com'}))
