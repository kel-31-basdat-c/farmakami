from django.shortcuts import render, redirect
from django.db import connection
from .forms import ApotekForm

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def list_apotek(request):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM apotek')
        list_apotek = dictfetchall(cursor)
    context = { 'list_apotek': list_apotek }
    return render(request, 'apotek.html', context)

def form_apotek(request):
    context = {}
    if request.method == 'POST':
        context['form'] = ApotekForm(request.POST)
        context['id_apotek'] = request.POST['id_apotek']
        context['action'] = 'Update'
    else:
        context['form'] = ApotekForm()
        context['action'] = 'Create'
    return render(request, 'form-apotek.html', context)

def register_apotek(request):
    if request.method == 'POST':
        nama_apotek = request.POST['nama_apotek']
        alamat_apotek = request.POST['alamat_apotek']
        telepon_apotek = request.POST['telepon_apotek']
        nama_penyelenggara = request.POST['nama_penyelenggara']
        no_sia = request.POST['no_sia']
        email = request.POST['email']
        if request.POST['action'] == 'Update':
            id_apotek = request.POST['id_apotek']
            with connection.cursor() as cursor:
                cursor.execute('UPDATE apotek SET ' \
                    + 'nama_apotek = %s, alamat_apotek = %s, ' \
                    + 'telepon_apotek = %s, no_sia = %s, email = %s, ' \
                    + 'nama_penyelenggara = %s ' \
                    + 'WHERE id_apotek = %s', \
                    [nama_apotek, alamat_apotek, telepon_apotek, no_sia, email, 
                    nama_penyelenggara, id_apotek])
        elif request.POST['action'] == 'Create':
            with connection.cursor() as cursor:
                cursor.execute('SELECT id_apotek FROM apotek ORDER BY id_apotek DESC LIMIT 1')
                last_id = cursor.fetchone()[0]
                order = int(last_id[5:]) + 1
                id_apotek = 'APTK-%0.4d' % order
                cursor.execute('INSERT INTO apotek VALUES (%s, %s, %s, %s, %s, %s, %s)', \
                    [id_apotek, email, no_sia, nama_penyelenggara, nama_apotek, alamat_apotek, telepon_apotek])
        return redirect('apotek:list')
    else:
        return redirect('apotek:form')

def delete_apotek(request):
    if request.method == 'POST':
        id_apotek = request.POST['id_apotek']
        with connection.cursor() as cursor:
            cursor.execute('DELETE FROM apotek WHERE id_apotek = %s', [id_apotek])
        return redirect('apotek:list')
    else:
        return render(request, 'notfound.html')