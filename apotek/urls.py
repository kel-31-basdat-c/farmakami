from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'apotek'

urlpatterns = [
    path('', views.list_apotek, name="list"),
    path('form', views.form_apotek, name="form"),
    path('register', views.register_apotek, name="register"),
    path('delete', views.delete_apotek, name="delete"),
]