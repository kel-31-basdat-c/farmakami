from django.shortcuts import render, redirect
from django.db import connection
from .forms import ProdukApotekForm

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def list_produk_apotek(request):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM produk_apotek')
        list_produk_apotek = dictfetchall(cursor)
    context = {
        'list_produk_apotek':list_produk_apotek
    }
    return render(request, 'produk-apotek.html', context)

def form_produk_apotek(request):
    context = {}
    
    # second time filling form
    if 'postdata' in request.session:
        data = request.session['postdata']
        del request.session['postdata']
        context['form'] = ProdukApotekForm(data)
        context['id_produk'] = data['old_id_produk']
        context['id_apotek'] = data['old_id_apotek']
        context['action'] = data['action']
        context['error'] = "ID produk dan ID apotek sudah terambil."

    #first time filling form
    elif request.method == 'POST':
        context['form'] = ProdukApotekForm(request.POST)
        context['id_produk'] = request.POST['id_produk']
        context['id_apotek'] = request.POST['id_apotek']
        context['action'] = 'Update'
    else:
        context['form'] = ProdukApotekForm()
        context['action'] = 'Create'
        
    return render(request, 'form-produk-apotek.html', context)

def register_produk_apotek(request):
    if request.method == 'POST':
        id_produk = request.POST['id_produk']
        id_apotek = request.POST['id_apotek']
        old_id_produk = request.POST['old_id_produk']
        old_id_apotek = request.POST['old_id_apotek']
        print(id_produk, id_apotek, old_id_apotek, old_id_produk)

        if old_id_apotek != id_apotek or old_id_produk != id_produk:
            # check if primary key exists => error
            with connection.cursor() as cursor:
                cursor.execute('SELECT * FROM produk_apotek WHERE id_apotek = %s AND id_produk = %s',
                [id_apotek, id_produk])
                row = cursor.fetchone()
                if row:
                    request.session['postdata'] = request.POST
                    return redirect('produk_apotek:form')

        harga_jual = request.POST['harga_jual']
        satuan_penjualan = request.POST['satuan_penjualan']
        stok = request.POST['stok']
        if request.POST['action'] == 'Update':
            old_id_produk = request.POST['old_id_produk']
            old_id_apotek = request.POST['old_id_apotek']
            with connection.cursor() as cursor:
                cursor.execute('UPDATE produk_apotek SET ' \
                    + 'id_apotek = %s, id_produk = %s, ' \
                    + 'harga_jual = %s, stok = %s, ' \
                    + 'satuan_penjualan = %s ' \
                    + 'WHERE id_apotek = %s AND id_produk = %s', \
                    [id_apotek, id_produk, harga_jual, stok, satuan_penjualan, 
                    old_id_apotek, old_id_produk])
        elif request.POST['action'] == 'Create':
            with connection.cursor() as cursor:
                cursor.execute('INSERT INTO produk_apotek VALUES (%s, %s, %s, %s, %s)', \
                    [harga_jual, stok, satuan_penjualan, id_produk, id_apotek])
        return redirect('produk_apotek:list')
    else:
        return redirect('produk_apotek:form')

def delete_produk_apotek(request):
    if request.method == 'POST':
        id_produk = request.POST['id_produk']
        id_apotek = request.POST['id_apotek']
        with connection.cursor() as cursor:
            cursor.execute('DELETE FROM produk_apotek WHERE id_apotek = %s AND id_produk = %s', \
                [id_apotek, id_produk])
        return redirect('produk_apotek:list')
    else:
        return render(request, 'notfound.html')