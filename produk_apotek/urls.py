from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'produk_apotek'

urlpatterns = [
    path('', views.list_produk_apotek, name="list"),
    path('form', views.form_produk_apotek, name="form"),
    path('register', views.register_produk_apotek, name="register"),
    path('delete', views.delete_produk_apotek, name="delete"),
]