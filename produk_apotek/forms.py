from django import forms
from django.db import connection

produk_choices = []
with connection.cursor() as cursor:
    cursor.execute('SELECT id_produk FROM produk')
    row = cursor.fetchall()
for id_produk in row:
    produk_choices.append((id_produk[0], id_produk[0]))

apotek_choices = []
with connection.cursor() as cursor:
    cursor.execute('SELECT id_apotek FROM apotek')
    row = cursor.fetchall()
for id_apotek in row:
    apotek_choices.append((id_apotek[0], id_apotek[0]))

class ProdukApotekForm(forms.Form):
 
    id_produk = forms.ChoiceField(label="ID Produk", widget=forms.Select(), 
        choices=produk_choices, initial=produk_choices[0][0])
    id_apotek = forms.ChoiceField(label="ID Apotek", widget=forms.Select(), 
        choices=apotek_choices, initial=apotek_choices[0][0])
    harga_jual = forms.CharField(label="Harga Jual", required=True, 
        widget=forms.NumberInput(attrs={'min':0}), initial=0)
    satuan_penjualan = forms.CharField(label="Satuan Penjualan", required=True, max_length=5, 
        widget=forms.TextInput(attrs={'placeholder': 'Tblt (untuk tablet)'}))
    stok = forms.CharField(label="Stok", required=True, 
        widget=forms.NumberInput(attrs={'min':0}), initial=0)
