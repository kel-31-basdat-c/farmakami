from django.shortcuts import render, redirect
from django.db import connection
from django.utils.datastructures import MultiValueDictKeyError

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def list_obat(request):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM obat ORDER BY id_obat ASC')
        list_obat = dictfetchall(cursor)
    result = {
        'list_obat' : list_obat
    }

    return render(request, 'obat.html', result)

def tambah_obat(request):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM merk_obat ORDER BY nama_dagang ASC')
        list_merk_obat = dictfetchall(cursor)

        last_id = []

        cursor.execute('SELECT id_obat FROM obat ORDER BY id_obat DESC LIMIT 1')
        temp_obat = cursor.fetchone()[0]

        last_id.insert(0, int(temp_obat[:5]+""+temp_obat[6:]) + 1)

        last_id[0] = "%s-%s" % (str(last_id[0])[:5], str(last_id[0])[5:])

        cursor.execute('SELECT id_produk FROM produk ORDER BY id_produk DESC LIMIT 1')
        temp_produk = cursor.fetchone()[0]

        last_id.insert(1, int(temp_produk[3:]) + 1)
        last_id[1] = 'PR-%d' % last_id[1]

    if request.method == 'POST':
        if 'id_merk_obat' not in request.POST.keys():
            is_error = True
            pesan = {
                'is_error' : is_error,
                'list_merk_obat' : list_merk_obat
            }
            return render(request, 'tambahobat.html', pesan)
        id_obat = last_id[0]
        id_produk = last_id[1]
        id_merk_obat = request.POST['id_merk_obat'][:7]
        netto = request.POST['netto']
        dosis = request.POST['dosis']
        aturan = request.POST['aturan']
        kontra = request.POST['kontra']
        bentuk = request.POST['bentuk']

        if aturan == '':
            aturan = None;
        if kontra == '':
            kontra = None;

        hasil = {
            'id_obat' : last_id[0],
            'id_produk' : last_id[1],
            'id_merk_obat' : id_merk_obat,
            'netto' : netto,
            'dosis' : dosis,
            'aturan' : aturan,
            'kontra' : kontra,
            'bentuk' : bentuk
        }
        
        # print(hasil)
        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO produk VALUES (%s)', [id_produk])
            cursor.execute('INSERT INTO obat VALUES (%s, %s, %s, %s, %s, %s, %s, %s)', \
                [last_id[0], last_id[1], id_merk_obat, netto, dosis, aturan, kontra, bentuk])
        return redirect('obat:list')
    else:
        daftar = {
        'list_merk_obat' : list_merk_obat
        }
        # print(daftar)
    return render(request, 'tambahobat.html', daftar)

def update_obat(request):
    if request.method == 'POST':
        id_obat = request.POST['id_obat']
        id_produk = request.POST['id_produk']
        id_merk_obat = request.POST['id_merk_obat'][:7]
        netto = request.POST['netto']
        dosis = request.POST['dosis']
        aturan = request.POST['aturan']
        kontra = request.POST['kontra']
        bentuk = request.POST['bentuk']

        if aturan == '':
            aturan = None;
        if kontra == '':
            kontra = None;
        
        hasil = {
            'id_obat' : id_obat,
            'id_produk' : id_produk,
            'id_merk_obat' : id_merk_obat,
            'netto' : netto,
            'dosis' : dosis,
            'aturan' : aturan,
            'kontra' : kontra,
            'bentuk' : bentuk
        }

        # print(hasil)
        with connection.cursor() as cursor:
            cursor.execute('UPDATE obat SET ' \
                + 'id_merk_obat = %s, netto = %s, ' \
                + 'dosis = %s, aturan_pakai = %s, '\
                + 'kontraindikasi = %s, bentuk_kesediaan = %s '\
                + 'WHERE id_obat = %s',\
                [id_merk_obat, netto, dosis, aturan, kontra, bentuk, id_obat])
        return redirect('obat:list')
    else:
        try:
            id_obat = request.GET['id_obat']
            with connection.cursor() as cursor:
                cursor.execute('SELECT * FROM obat WHERE id_obat = %s', [id_obat])
                list_update = dictfetchall(cursor)

                cursor.execute('SELECT * FROM merk_obat ORDER BY nama_dagang ASC')
                list_merk_obat = dictfetchall(cursor)

            if id_produk == None:
                return render(request, 'updateobat.html', {'is_failed' : True})

            daftar = {
                'list_update' : list_update,
                'list_merk_obat' : list_merk_obat
            }
        except:
            daftar = {
                'is_failed' : True
            }
        
    return render(request, 'updateobat.html', daftar)

def delete_obat(request):
    try:
        id_obat = request.POST['id_obat']
        id_produk = request.POST['id_produk']
        with connection.cursor() as cursor:
            cursor.execute('DELETE FROM produk WHERE id_produk = %s', [id_produk])
            cursor.execute('DELETE FROM obat WHERE id_obat = %s', [id_obat])
        return redirect('obat:list')
    except:
        return render(request, 'notfound.html')
    