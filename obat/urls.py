from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'obat'

urlpatterns = [
    path('', views.list_obat, name="list"),
    path('create', views.tambah_obat, name='tambah_obat'),
    path('update', views.update_obat, name='update_obat'),
    path('delete', views.delete_obat, name="delete_obat")
]