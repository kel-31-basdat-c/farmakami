from django.shortcuts import render, redirect
from django.db import connection
from .forms import LoginForm

def login(request):
    context = {}
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        with connection.cursor() as cursor:
            cursor.execute('SELECT password, tipe FROM pengguna WHERE email = %s', [email])
            row = cursor.fetchone()
            if row and row[0] == password:
                request.session['auth'] = email
                request.session['role'] = row[1]
                return redirect('profil:profil')
            elif row and row[0] != password:
                context['form'] = LoginForm(request.POST)
                context['error'] = "Email atau password salah."
                return render(request, 'login.html', context)
            else:
                context['form'] = LoginForm(request.POST)
                context['error'] = "Email belum terdaftar."
                return render(request, 'login.html', context)
    else:
        context['form'] = LoginForm
        return render(request, 'login.html', context)

def logout(request):
    request.session.flush()
    return redirect('authentication:login')

def handler404(request, exception):
    response = render(request, "notfound.html")
    response.status_code = 404
    return response

def handler500(request):
    response = render(request, "servererror.html")
    response.status_code = 500
    return response
