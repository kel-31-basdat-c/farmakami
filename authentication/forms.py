from django import forms

class LoginForm(forms.Form):
    email = forms.CharField(label="Email", required=True, max_length=50, 
        widget=forms.EmailInput(attrs={ 'placeholder': 'farmakami@gmail.com' }))
    password = forms.CharField(label="Password", required=True, max_length=128, widget=forms.PasswordInput())
