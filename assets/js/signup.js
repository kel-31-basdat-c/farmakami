$(document).ready(function() {
    var max_fields = 5;
    var current_fields = 1;
    $('#addimg').click(function(e) {
        if (current_fields < max_fields) {
            current_fields++;
            var input_status = '<input type="text" name="status' + current_fields + '" class="inline-flex third-width" placeholder="aktif">';
            var input_alamat = '<input type="text" name="alamat' + current_fields + '" class="inline-flex" placeholder="Jl. Basdat C">';
            var input = '<div class="input-address">' + input_status + input_alamat + '</div>'
            $('.form').append(input);
        } 
        if (current_fields === max_fields) {
            $('#add_image').remove();
        }
    });
});